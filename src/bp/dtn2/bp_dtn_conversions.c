/** \file bp_dtn_conversions.c
 *
 *  \brief  This files contains the functions that convert Unified API abstract types (al_types) into
 *   dtn2/dtnme types and vice versa.
 *
 *  \par Copyright
 *  	Copyright (c) 2013, Alma Mater Studiorum, University of Bologna. All rights reserved.
 *
 *  \par License
 *     This file is part of Unified-API.                                            	<br>
 *                                                                                	<br>
 *     Unified-API is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.                                        	<br>
 *     Unified-API is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.                               	<br>
 *                                                                                	<br>
 *     You should have received a copy of the GNU General Public License
 *     along with Unified-API.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  \authors Michele Rodolfi, michele.rodolfi@studio.unibo.it
 *  \authors Anna d'Amico, anna.damico2@studio.unibo.it
 *  \authors Silvia Lanzoni, silvia Lanzoni5@studio.unibo.it
 *
 *  \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 *	\par Revision History
 *
 *  DD/MM/YY | AUTHOR          	|  DESCRIPTION
 *  -------- | --------------- 	| -----------------------------------------------
 *  01/01/13 | M. Rodolfi	|  Initial Implementation.
 *  01/01/13 | A. D'Amico	|  Initial Implementation.
 *  01/10/21 | S. Lanzoni       |  Function prefixes changed (e.g. bp_from_dtn, bp_to_dtn).
 *  07/11/22 | L. Persampieri   |  Added compatibility with DTNME 1.2.0 and later.
 *  11/08/23 | L. Fantini       |  Removed memcpy of payload
 */
//#define DTN2_IMPLEMENTATION
#ifdef DTN2_IMPLEMENTATION

#include "bp_dtn_conversions.h"

//Define a macro to add the correct timestmap time field, depending on DTNME version ("secs" in old versions, "secs_or_millisecs" in new ones)

//#define DTN2_OR_DTNME_OLD 1 
#ifdef SECS //(DTN2 and DTNME<1.2.0)
#define add_dtnme_timestamp_time_field(dtn_timestamp) dtn_timestamp.secs
#else //DTNME >=1.2.0
#define add_dtnme_timestamp_time_field(dtn_timestamp) dtn_timestamp.secs_or_millisecs
#endif
 

dtn_handle_t bp_to_dtn_handle(al_types_handle handle)
{
	return (dtn_handle_t) handle;
}
al_types_handle bp_from_dtn_handle(dtn_handle_t handle)
{
	return (al_types_handle) handle;
}

dtn_endpoint_id_t bp_to_dtn_endpoint_id(al_types_endpoint_id endpoint_id)
{
	dtn_endpoint_id_t dtn_eid;
	strncpy(dtn_eid.uri, endpoint_id.uri, DTN_MAX_ENDPOINT_ID);
	return dtn_eid;
}
al_types_endpoint_id bp_from_dtn_endpoint_id(dtn_endpoint_id_t endpoint_id)
{
	al_types_endpoint_id bp_eid;
	strncpy(bp_eid.uri, endpoint_id.uri, AL_TYPES_MAX_EID_LENGTH);
	return bp_eid;
}

dtn_timeval_t bp_to_dtn_timeval(al_types_timeval timeval)
{
	return (dtn_timeval_t) timeval;
}
al_types_timeval bp_from_dtn_timeval(dtn_timeval_t timeval)
{
	return (al_types_timeval) timeval;
}

dtn_timeval_t bp_to_dtn_timeout(al_types_timeout timeout){
	return (dtn_timeval_t) timeout; // milliseconds
}
al_types_timeout bp_from_dtn_timeout(dtn_timeval_t timeout){
	return (al_types_timeout) timeout; // milliseconds
}

dtn_timestamp_t bp_to_dtn_timestamp(al_types_creation_timestamp timestamp)
{
	dtn_timestamp_t dtn_timestamp;
//	dtn_timestamp.secs = timestamp.time;
	add_dtnme_timestamp_time_field(dtn_timestamp) = timestamp.time;
	dtn_timestamp.seqno = timestamp.seqno;
	return dtn_timestamp;
}
al_types_creation_timestamp bp_from_dtn_timestamp(dtn_timestamp_t timestamp)
{
	al_types_creation_timestamp bp_timestamp;
//	bp_timestamp.time = timestamp.secs;
	bp_timestamp.time = add_dtnme_timestamp_time_field(timestamp);
	bp_timestamp.seqno = timestamp.seqno;
	return bp_timestamp;
}

dtn_reg_token_t bp_to_dtn_reg_token(al_types_reg_token reg_token)
{
	return (dtn_reg_token_t) reg_token;
}
al_types_reg_token bp_from_dtn_reg_token(dtn_reg_token_t reg_token)
{
	return (al_types_reg_token) reg_token;

}

dtn_reg_id_t bp_to_dtn_reg_id(al_types_reg_id reg_id)
{
	return (dtn_reg_id_t) reg_id;
}
al_types_reg_id bp_from_dtn_reg_id(dtn_reg_id_t reg_id)
{
	return (al_types_reg_id) reg_id;
}

dtn_reg_info_t bp_to_dtn_reg_info(al_types_reg_info reg_info)
{
	dtn_reg_info_t dtn_reginfo;
	memset(&dtn_reginfo, 0, sizeof(dtn_reg_info_t));
	dtn_reginfo.endpoint = bp_to_dtn_endpoint_id(reg_info.endpoint);
	dtn_reginfo.regid = bp_to_dtn_reg_id(reg_info.regid);
	dtn_reginfo.flags = reg_info.flags;
	dtn_reginfo.replay_flags = reg_info.replay_flags;
	dtn_reginfo.expiration = bp_to_dtn_timeval(reg_info.expiration);
	dtn_reginfo.reg_token = bp_to_dtn_reg_token(reg_info.reg_token);
	dtn_reginfo.init_passive = reg_info.init_passive;
	dtn_reginfo.script.script_len = reg_info.script.script_len;
	if (reg_info.script.script_len == 0)
	{
		dtn_reginfo.script.script_val = NULL;
	}
	else
	{
		dtn_reginfo.script.script_val = (char*) malloc(reg_info.script.script_len + 1);
		strncpy(dtn_reginfo.script.script_val, reg_info.script.script_val, reg_info.script.script_len + 1);
	}
	return dtn_reginfo;
}
al_types_reg_info bp_from_dtn_reg_info(dtn_reg_info_t reg_info)
{
	al_types_reg_info bp_reginfo;
	memset(&bp_reginfo, 0, sizeof(al_types_reg_info));
	bp_reginfo.endpoint = bp_from_dtn_endpoint_id(reg_info.endpoint);
	bp_reginfo.regid = bp_from_dtn_reg_id(reg_info.regid);
	bp_reginfo.flags = reg_info.flags;
	bp_reginfo.replay_flags = reg_info.replay_flags;
	bp_reginfo.expiration = bp_from_dtn_timeval(reg_info.expiration);
	bp_reginfo.reg_token = bp_from_dtn_reg_token(reg_info.reg_token);
	bp_reginfo.init_passive = reg_info.init_passive;
	bp_reginfo.script.script_len = reg_info.script.script_len;
	if (reg_info.script.script_len == 0)
	{
		bp_reginfo.script.script_val = NULL;
	}
	else
	{
		bp_reginfo.script.script_val = (char*) malloc(reg_info.script.script_len + 1);
		strncpy(bp_reginfo.script.script_val, reg_info.script.script_val, reg_info.script.script_len + 1);
	}
	return bp_reginfo;
}

dtn_reg_flags_t bp_to_dtn_reg_flags(al_types_reg_flags reg_flags)
{
	return (dtn_reg_flags_t) reg_flags;
}
al_types_reg_flags bp_from_dtn_reg_flags(dtn_reg_flags_t reg_flags)
{
	return (al_types_reg_flags) reg_flags;
}

dtn_bundle_delivery_opts_t bp_to_dtn_bundle_delivery_opts(al_types_bundle_processing_control_flags bundle_delivery_opts)
{
	return (dtn_bundle_delivery_opts_t) bundle_delivery_opts;
}
al_types_bundle_processing_control_flags bp_from_dtn_bundle_delivery_opts(dtn_bundle_delivery_opts_t bundle_delivery_opts)
{
	return (al_types_bundle_processing_control_flags) bundle_delivery_opts;
}

dtn_bundle_priority_t bp_to_dtn_bundle_priority(al_types_bundle_priority_enum bundle_priority)
{
	return (dtn_bundle_priority_t) bundle_priority;
}

al_types_bundle_priority_enum dtn_al_bundle_priority(dtn_bundle_priority_t bundle_priority)
{
	al_types_bundle_priority_enum bp_priority;
	bp_priority = (al_types_bundle_priority_enum) bundle_priority;
	//bp_priority.ordinal = 0;
	return bp_priority;
}

dtn_bundle_spec_t bp_to_dtn_bundle_spec(al_types_bundle_spec bundle_spec)
{
	dtn_bundle_spec_t dtn_bundle_spec;
	int i;
	al_types_extension_block dtn_bundle_block;
	memset(&dtn_bundle_spec, 0, sizeof(dtn_bundle_spec));
	memset(&dtn_bundle_block, 0, sizeof(dtn_bundle_block));
	dtn_bundle_spec.source = bp_to_dtn_endpoint_id(bundle_spec.source);
	dtn_bundle_spec.dest = bp_to_dtn_endpoint_id(bundle_spec.destination);
	dtn_bundle_spec.replyto = bp_to_dtn_endpoint_id(bundle_spec.report_to);
	dtn_bundle_spec.priority = bundle_spec.cardinal;
	dtn_bundle_spec.dopts = bp_to_dtn_bundle_delivery_opts(bundle_spec.bundle_proc_ctrl_flags);
	dtn_bundle_spec.expiration = bp_to_dtn_timeval(bundle_spec.lifetime);
	dtn_bundle_spec.creation_ts = bp_to_dtn_timestamp(bundle_spec.creation_ts);
	dtn_bundle_spec.delivery_regid = bp_to_dtn_reg_id(bundle_spec.delivery_regid);
	
	// XXX Work in progress to support new DTNME APIs
	dtn_bundle_spec.ecos_enabled = bundle_spec.ecos.ecos_enabled;
	if ( bundle_spec.ecos.ecos_enabled ) {
			dtn_bundle_spec.ecos_flags = (ECOS_FLAG_FLOW_LABEL | (bundle_spec.ecos.critical ? ECOS_FLAG_CRITICAL : 0) | (bundle_spec.ecos.unreliable ? ECOS_FLAG_STREAMING : 0)| (bundle_spec.ecos.reliable ? ECOS_FLAG_RELIABLE : 0)); // CC era sbagliato
			dtn_bundle_spec.ecos_ordinal = bundle_spec.ecos.ordinal;
			dtn_bundle_spec.ecos_flow_label = bundle_spec.ecos.flow_label;
	} else {
		dtn_bundle_spec.ecos_flags = 0;
		dtn_bundle_spec.ecos_ordinal = 0;
		dtn_bundle_spec.ecos_flow_label = 0;
	}
	dtn_bundle_spec.bp_version = bundle_spec.bp_version;

	//extension blocks
	if(bundle_spec.extensions.extension_number == 0) {
	dtn_bundle_spec.blocks.blocks_val = NULL;
	dtn_bundle_spec.metadata.metadata_val = NULL;
	}
	else{
		//malloc
		dtn_bundle_spec.blocks.blocks_val =
						(dtn_extension_block_t*) malloc(bundle_spec.extensions.extension_number);
		dtn_bundle_spec.metadata.metadata_val =
						(dtn_extension_block_t*) malloc(bundle_spec.extensions.extension_number);
		//distinguish metadata and other blocks using type
		 for(i=0; i<bundle_spec.extensions.extension_number; i++)
			    {
			 	 	 if( bundle_spec.extensions.extension_blocks[i].block_type_code == 8) { //metadata
			 	 		 //prepare fields
			 			char backup_specific_data[1000]; //to be fixed
			 			strcpy (backup_specific_data,  bundle_spec.extensions.extension_blocks[i].block_data.block_type_specific_data );
			 			//printf("\n\n%ld", strlen(backup_specific_data));
			 			char * metadata_type_string = strtok (backup_specific_data, "/");
			 			//printf("\n\n%ld", strlen(metadata_type_string));
			 			uint32_t metadata_type = atoi(metadata_type_string);
			 			char metadata_string [1000];
			 			int total_length = (int) strlen(bundle_spec.extensions.extension_blocks[i].block_data.block_type_specific_data ) - 1;
			 			int partial_length = (int) strlen(metadata_type_string);
			 			if(total_length != partial_length){
			 				char * temp_metadata_string = strtok (NULL, "\0");
			 				strcpy (metadata_string,temp_metadata_string);
			 			}
			 	 		uint32_t datalen = strlen(metadata_string);

			 	 		dtn_bundle_spec.metadata.metadata_len ++;
			 	    	dtn_bundle_block = bundle_spec.extensions.extension_blocks[i];
			 	        dtn_bundle_spec.metadata.metadata_val[i].type = metadata_type;
			 	        dtn_bundle_spec.metadata.metadata_val[i].flags = dtn_bundle_block.block_processing_control_flags;
			 	        dtn_bundle_spec.metadata.metadata_val[i].data.data_len = datalen;
			 	        if(datalen == 0)
			 	        	dtn_bundle_spec.metadata.metadata_val[i].data.data_val = NULL;
			 	        else
			 	        {
			 	        	dtn_bundle_spec.metadata.metadata_val[i].data.data_val=
			 	        			(char*) malloc(datalen + 1);
			 	            memcpy(dtn_bundle_spec.metadata.metadata_val[i].data.data_val,
			 	            		metadata_string, (datalen) + 1);
			 	            dtn_bundle_spec.metadata.metadata_val[i].data.data_val =
			 	            		(char*)metadata_string;
			 	        }
			 	 	 }
			 	 	 else{ //extension_blocks
			 	 		 	 	 	 dtn_bundle_block = bundle_spec.extensions.extension_blocks[i];
			 	 		 	 	 	 dtn_bundle_spec.blocks.blocks_len ++;
			 	 			        dtn_bundle_spec.blocks.blocks_val[i].type = dtn_bundle_block.block_type_code;
			 	 			        dtn_bundle_spec.blocks.blocks_val[i].flags = dtn_bundle_block.block_processing_control_flags;
			 	 			        dtn_bundle_spec.blocks.blocks_val[i].data.data_len = dtn_bundle_block.block_data.block_type_specific_data_len;
			 	 			        if(dtn_bundle_block.block_data.block_type_specific_data_len == 0)
			 	 			        	dtn_bundle_spec.blocks.blocks_val[i].data.data_val = NULL;
			 	 			        else
			 	 			        {
			 	 			        	dtn_bundle_spec.blocks.blocks_val[i].data.data_val =
			 	 			        			(char*) malloc(dtn_bundle_block.block_data.block_type_specific_data_len + 1);
			 	 			            memcpy(dtn_bundle_spec.blocks.blocks_val[i].data.data_val,
			 	 			            		dtn_bundle_block.block_data.block_type_specific_data, (dtn_bundle_block.block_data.block_type_specific_data_len) + 1);
			 	 			            dtn_bundle_spec.blocks.blocks_val[i].data.data_val =
			 	 			            		(char*)dtn_bundle_block.block_data.block_type_specific_data;

			 	 	 }
			 	 	 }
			    }
	}
	return dtn_bundle_spec;
}
al_types_bundle_spec bp_from_dtn_bundle_spec(dtn_bundle_spec_t bundle_spec)
{
	al_types_bundle_spec bp_bundle_spec;
	int i;
	int k =0;
	dtn_extension_block_t bp_bundle_block;
	memset(&bp_bundle_spec, 0, sizeof(bp_bundle_spec));
	memset(&bp_bundle_block, 0, sizeof(bp_bundle_block));
	bp_bundle_spec.source = bp_from_dtn_endpoint_id(bundle_spec.source);
	bp_bundle_spec.destination = bp_from_dtn_endpoint_id(bundle_spec.dest);
	bp_bundle_spec.report_to = bp_from_dtn_endpoint_id(bundle_spec.replyto);
	bp_bundle_spec.cardinal = bundle_spec.priority;
	bp_bundle_spec.bundle_proc_ctrl_flags = bp_from_dtn_bundle_delivery_opts(bundle_spec.dopts);
	bp_bundle_spec.lifetime = bp_from_dtn_timeval(bundle_spec.expiration);
	bp_bundle_spec.creation_ts = bp_from_dtn_timestamp(bundle_spec.creation_ts);
	bp_bundle_spec.delivery_regid = bp_from_dtn_reg_id(bundle_spec.delivery_regid);
	
	// XXX Work in progress to support new DTNME APIs
	bp_bundle_spec.ecos.ecos_enabled = bundle_spec.ecos_enabled;
	if ( bp_bundle_spec.ecos.ecos_enabled ) {
		bp_bundle_spec.ecos.unreliable = bundle_spec.ecos_flags & ECOS_FLAG_STREAMING;
	    bp_bundle_spec.ecos.reliable = bundle_spec.ecos_flags & ECOS_FLAG_RELIABLE;
		bp_bundle_spec.ecos.critical = bundle_spec.ecos_flags & ECOS_FLAG_CRITICAL;
		bp_bundle_spec.ecos.ordinal = bundle_spec.ecos_ordinal;
		bp_bundle_spec.ecos.flow_label = bundle_spec.ecos_flags & ECOS_FLAG_FLOW_LABEL ? bundle_spec.ecos_flow_label : 0;
	} else {
		bp_bundle_spec.ecos.unreliable = 0;
		bp_bundle_spec.ecos.reliable = 0;
		bp_bundle_spec.ecos.critical = 0;
		bp_bundle_spec.ecos.ordinal = 0;
		bp_bundle_spec.ecos.flow_label = 0;
	}
	bp_bundle_spec.bp_version = bundle_spec.bp_version;
	
	// METADATA AND EXTENSION BLOCKS
	if(bundle_spec.metadata.metadata_len == 0 && bundle_spec.blocks.blocks_len == 0) //both zero
		bp_bundle_spec.extensions.extension_blocks = NULL;
	else
	{
		bp_bundle_spec.extensions.extension_number = bundle_spec.metadata.metadata_len + bundle_spec.blocks.blocks_len; //total number of blocks
		//alloc memory for both
		bp_bundle_spec.extensions.extension_blocks =
//dz debug				(al_types_bundle_priority*) malloc(bundle_spec.blocks.blocks_len);
				(al_types_extension_block*) malloc(bp_bundle_spec.extensions.extension_number * sizeof(al_types_extension_block));

		//extension blocks
	    for(i=0; i< bundle_spec.blocks.blocks_len; i++)
	    {
	    	bp_bundle_block = bundle_spec.blocks.blocks_val[i];
	        bp_bundle_spec.extensions.extension_blocks[i].block_type_code = bp_bundle_block.type;
	        bp_bundle_spec.extensions.extension_blocks[i].block_processing_control_flags = bp_bundle_block.flags;
	        bp_bundle_spec.extensions.extension_blocks[i].block_data.block_type_specific_data_len = bp_bundle_block.data.data_len;
	        if(bp_bundle_block.data.data_len == 0)
	        	bp_bundle_spec.extensions.extension_blocks[i].block_data.block_type_specific_data = NULL;
	        else
	        {
//dz debug	        	bp_bundle_spec.blocks.blocks_val[i].data.data_val =
//dz debug	        			(char*) malloc(bp_bundle_block.data.data_len + 1);
//dz debug	            memcpy(bp_bundle_spec.blocks.blocks_val[i].data.data_val,
//dz debug	            		bp_bundle_block.data.data_val, (bp_bundle_block.data.data_len) + 1);
	        	bp_bundle_spec.extensions.extension_blocks[i].block_data.block_type_specific_data=
	        			(char*) malloc(bp_bundle_block.data.data_len);
	            memcpy(bp_bundle_spec.extensions.extension_blocks[i].block_data.block_type_specific_data,
	            		bp_bundle_block.data.data_val, bp_bundle_block.data.data_len);

//XXX/dz - Just copied the data above so this is not needed and would overwrite the data_val pointerr 
//dz debug	            bp_bundle_spec.blocks.blocks_val[i].data.data_val =
//dz debug	            		(char*)bp_bundle_block.data.data_val;
	        }
	        k++;
	    }
	}
	    for(i =0 ; i< bundle_spec.metadata.metadata_len; i++)
	    {
	    	bp_bundle_block = bundle_spec.metadata.metadata_val[i];
	        bp_bundle_spec.extensions.extension_blocks[k].block_type_code = bp_bundle_block.type;
	        bp_bundle_spec.extensions.extension_blocks[k].block_processing_control_flags = bp_bundle_block.flags;
	        bp_bundle_spec.extensions.extension_blocks[k].block_data.block_type_specific_data_len = bp_bundle_block.data.data_len;
	        if(bp_bundle_block.data.data_len == 0)
	        	bp_bundle_spec.extensions.extension_blocks[k].block_data.block_type_specific_data = NULL;
	        else
	        {
	        	bp_bundle_spec.extensions.extension_blocks[k].block_data.block_type_specific_data=
	        			(char*) malloc(bp_bundle_block.data.data_len + 1);
	            memcpy(bp_bundle_spec.extensions.extension_blocks[k].block_data.block_type_specific_data,
	            		bp_bundle_block.data.data_val, (bp_bundle_block.data.data_len) + 1);
	            bp_bundle_spec.extensions.extension_blocks[k].block_data.block_type_specific_data =
	            		(char*)bp_bundle_block.data.data_val;
	        }
	        k++;
	    }
	return bp_bundle_spec;
}

dtn_bundle_payload_location_t bp_to_dtn_bundle_payload_location(al_types_bundle_payload_location bundle_payload_location)
{
	return (dtn_bundle_payload_location_t) bundle_payload_location;
}
al_types_bundle_payload_location bp_from_dtn_bundle_payload_location(dtn_bundle_payload_location_t bundle_payload_location)
{
	return (al_types_bundle_payload_location) bundle_payload_location;
}

dtn_status_report_reason_t bp_to_dtn_status_report_reason(al_types_status_report_reason status_report_reason)
{
	return (dtn_status_report_reason_t) status_report_reason;
}
al_types_status_report_reason bp_from_dtn_status_report_reason(dtn_status_report_reason_t status_report_reason)
{
	return (al_types_status_report_reason) status_report_reason;
}

dtn_status_report_flags_t bp_to_dtn_status_report_flags(al_types_status_report_flags status_report_flags)
{
	return (dtn_status_report_flags_t) status_report_flags;
}
al_types_status_report_flags bp_from_dtn_status_report_flags(dtn_status_report_flags_t status_report_flags)
{
	return (al_types_status_report_flags) status_report_flags;
}

dtn_bundle_id_t bp_to_dtn_bundle_id(al_types_bundle_id bundle_id)
{
	dtn_bundle_id_t dtn_bundle_id;
	dtn_bundle_id.source = bp_to_dtn_endpoint_id(bundle_id.source);
	dtn_bundle_id.creation_ts = bp_to_dtn_timestamp(bundle_id.creation_ts);
	if(bundle_id.frag_offset < 0)
		dtn_bundle_id.frag_offset = -1;
	else
		dtn_bundle_id.frag_offset = bundle_id.frag_offset;
	dtn_bundle_id.orig_length = bundle_id.frag_length;
	return dtn_bundle_id;
}
al_types_bundle_id bp_from_dtn_bundle_id(dtn_bundle_id_t bundle_id)
{
	al_types_bundle_id bp_bundle_id;
	bp_bundle_id.source = bp_from_dtn_endpoint_id(bundle_id.source);
	bp_bundle_id.creation_ts = bp_from_dtn_timestamp(bundle_id.creation_ts);
	if(bundle_id.frag_offset < 0)
		bp_bundle_id.frag_offset = -1;
	else
		bp_bundle_id.frag_offset = bundle_id.frag_offset;
	bp_bundle_id.frag_length = bundle_id.orig_length; //TODO: it should be bundle_id.frag_length
	return bp_bundle_id;
}

dtn_bundle_status_report_t bp_to_dtn_bundle_status_report(al_types_bundle_status_report bundle_status_report)
{
	dtn_bundle_status_report_t dtn_bundle_status_report;
	memset(&dtn_bundle_status_report, 0, sizeof(dtn_bundle_status_report_t));
	dtn_bundle_status_report.bundle_id = bp_to_dtn_bundle_id(bundle_status_report.bundle_x_id);
	dtn_bundle_status_report.reason = bp_to_dtn_status_report_reason(bundle_status_report.reason);
	dtn_bundle_status_report.flags = bp_to_dtn_status_report_flags(bundle_status_report.flags);

	add_dtnme_timestamp_time_field(dtn_bundle_status_report.receipt_ts) = bundle_status_report.reception_ts;
	add_dtnme_timestamp_time_field(dtn_bundle_status_report.custody_ts) = bundle_status_report.custody_ts;
	add_dtnme_timestamp_time_field(dtn_bundle_status_report.forwarding_ts) = bundle_status_report.forwarding_ts;
	add_dtnme_timestamp_time_field(dtn_bundle_status_report.delivery_ts) = bundle_status_report.delivery_ts;
	add_dtnme_timestamp_time_field(dtn_bundle_status_report.deletion_ts) = bundle_status_report.deletion_ts;
	add_dtnme_timestamp_time_field(dtn_bundle_status_report.ack_by_app_ts) = bundle_status_report.ack_by_app_ts;
/*
	dtn_bundle_status_report.custody_ts.secs = bundle_status_report.custody_ts;
	dtn_bundle_status_report.forwarding_ts.secs = bundle_status_report.forwarding_ts;
	dtn_bundle_status_report.delivery_ts.secs = bundle_status_report.delivery_ts;
	dtn_bundle_status_report.deletion_ts.secs = bundle_status_report.deletion_ts;
	dtn_bundle_status_report.ack_by_app_ts.secs = bundle_status_report.ack_by_app_ts; */

	return dtn_bundle_status_report;
}
al_types_bundle_status_report * bp_from_dtn_bundle_status_report(dtn_bundle_status_report_t bundle_status_report)
{
	al_types_bundle_status_report * bp_bundle_status_report = (al_types_bundle_status_report *) malloc(sizeof(al_types_bundle_status_report));
	memset(bp_bundle_status_report, 0, sizeof(al_types_bundle_status_report));
	//printf("AL_BP: fragment offset dtn %d\n",bundle_status_report.bundle_id.frag_offset);
	bp_bundle_status_report->bundle_x_id = bp_from_dtn_bundle_id(bundle_status_report.bundle_id);
	//printf("AL_BP: fragment offset al_bp %lu\n",bp_bundle_status_report.bundle_id.frag_offset);
	bp_bundle_status_report->reason = bp_from_dtn_status_report_reason(bundle_status_report.reason);
	bp_bundle_status_report->flags = bp_from_dtn_status_report_flags(bundle_status_report.flags);

	bp_bundle_status_report->reception_ts = add_dtnme_timestamp_time_field(bundle_status_report.receipt_ts);
	bp_bundle_status_report->custody_ts = add_dtnme_timestamp_time_field(bundle_status_report.custody_ts);
	bp_bundle_status_report->forwarding_ts = add_dtnme_timestamp_time_field(bundle_status_report.forwarding_ts);
	bp_bundle_status_report->delivery_ts = add_dtnme_timestamp_time_field(bundle_status_report.delivery_ts);
	bp_bundle_status_report->deletion_ts = add_dtnme_timestamp_time_field(bundle_status_report.deletion_ts);
	bp_bundle_status_report->ack_by_app_ts = add_dtnme_timestamp_time_field(bundle_status_report.ack_by_app_ts);

/*
	bp_bundle_status_report->reception_ts = bundle_status_report.receipt_ts.secs;
	bp_bundle_status_report->custody_ts = bundle_status_report.custody_ts.secs;
	bp_bundle_status_report->forwarding_ts = bundle_status_report.forwarding_ts.secs;
	bp_bundle_status_report->delivery_ts = bundle_status_report.delivery_ts.secs;
	bp_bundle_status_report->deletion_ts = bundle_status_report.deletion_ts.secs;
	bp_bundle_status_report->ack_by_app_ts = bundle_status_report.ack_by_app_ts.secs;*/

	return bp_bundle_status_report;
}

dtn_bundle_payload_t bp_to_dtn_bundle_payload(al_types_bundle_payload bundle_payload)
{
	dtn_bundle_payload_t dtn_bundle_payload;
	memset(&dtn_bundle_payload, 0, sizeof(dtn_bundle_payload));
	dtn_bundle_payload.location = bp_to_dtn_bundle_payload_location(bundle_payload.location);
	dtn_bundle_payload.filename.filename_len = bundle_payload.filename.filename_len;
	if (bundle_payload.filename.filename_len == 0)
	{
		dtn_bundle_payload.filename.filename_val = NULL;
	}
	else
	{
		// COPY ADDRESS of the filename string
		dtn_bundle_payload.filename.filename_val = bundle_payload.filename.filename_val;
		// dtn_bundle_payload.filename.filename_val = (char*) malloc(bundle_payload.filename.filename_len + 1);
		// strncpy(dtn_bundle_payload.filename.filename_val, bundle_payload.filename.filename_val, bundle_payload.filename.filename_len + 1);
	}
	dtn_bundle_payload.buf.buf_len = bundle_payload.buf.buf_len;
	if (bundle_payload.buf.buf_len == 0)
	{
		dtn_bundle_payload.buf.buf_val = NULL;
	}
	else
	{
		//dtn_bundle_payload.buf.buf_val=bundle_payload.buf.buf_val; da valutare con Bisacchi Faccio una copia del contenuto della memoria
		//anziché una copia del puntatore alla memoria allocata
		// dtn_bundle_payload.buf.buf_val = (char*) malloc(bundle_payload.buf.buf_len);
		// memcpy(dtn_bundle_payload.buf.buf_val, bundle_payload.buf.buf_val, bundle_payload.buf.buf_len);
		dtn_bundle_payload.buf.buf_val = bundle_payload.buf.buf_val;
	}
	if (bundle_payload.status_report == NULL)
	{
		dtn_bundle_payload.status_report = NULL;
	}
	else
	{
		dtn_bundle_status_report_t dtn_bundle_status_report = bp_to_dtn_bundle_status_report(*(bundle_payload.status_report));
		dtn_bundle_payload.status_report = & dtn_bundle_status_report;
	}
	return dtn_bundle_payload;
}

al_types_bundle_payload bp_from_dtn_bundle_payload(dtn_bundle_payload_t bundle_payload)
{
	al_types_bundle_payload bp_bundle_payload;
	memset(&bp_bundle_payload, 0, sizeof(bp_bundle_payload));
	bp_bundle_payload.location = bp_from_dtn_bundle_payload_location(bundle_payload.location);
	bp_bundle_payload.filename.filename_len = bundle_payload.filename.filename_len;
	if (bundle_payload.filename.filename_len == 0 || bundle_payload.filename.filename_val == NULL)//Non è su file
	{
		bp_bundle_payload.filename.filename_val = NULL;
		bp_bundle_payload.filename.filename_len = 0;
	}
	else //e' su file
	{
		// COPY ADDRESS of the filename string
		bp_bundle_payload.filename.filename_val = bundle_payload.filename.filename_val;
		// bp_bundle_payload.filename.filename_val = (char*) malloc(bundle_payload.filename.filename_len + 1);
		// strncpy(bp_bundle_payload.filename.filename_val, bundle_payload.filename.filename_val, bundle_payload.filename.filename_len + 1);
	}
	bp_bundle_payload.buf.buf_len = bundle_payload.buf.buf_len;
	if (bundle_payload.buf.buf_len == 0 || bundle_payload.buf.buf_val == NULL) //Non è in memoria
	{
		bp_bundle_payload.buf.buf_len = 0;
		bp_bundle_payload.buf.buf_val = NULL;
	}
	else //e' in memoria
	{
		//bp_bundle_payload.buf.buf_val=bundle_payload.buf.buf_val; CCaini da valutare con Bisacchi da valutare con Bisacchi Faccio una copia del contenuto della memoria
		//anziché una copia del puntatore alla memoria allocata
		// bp_bundle_payload.buf.buf_val = (char*) malloc(bundle_payload.buf.buf_len + 1);
		// memcpy(bp_bundle_payload.buf.buf_val, bundle_payload.buf.buf_val, bundle_payload.buf.buf_len);
		bp_bundle_payload.buf.buf_val = bundle_payload.buf.buf_val;
	}if (bundle_payload.status_report == NULL)
	{
		bp_bundle_payload.status_report = NULL;
	}
	else
	{
		al_types_bundle_status_report * bp_bundle_status_report = bp_from_dtn_bundle_status_report(*(bundle_payload.status_report));
		bp_bundle_payload.status_report = bp_bundle_status_report;
	}

	return bp_bundle_payload;
}
#endif /* DTN2_IMPLEMENTATION */
