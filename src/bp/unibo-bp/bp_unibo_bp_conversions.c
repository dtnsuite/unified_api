/** \file bp_unibo_bp_conversions.c
 *
 *  \brief  This file contains the functions that convert Unified API abstract types (al_types) into
 *          Unibo-BP types and vice versa.
 *
 *  \par Copyright
 *  	Copyright (c) 2022, Alma Mater Studiorum, University of Bologna. All rights reserved.
 *
 *  \par License
 *     This file is part of Unified-API.                                            	<br>
 *                                                                                	<br>
 *     Unified-API is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.                                        	<br>
 *     Unified-API is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.                               	<br>
 *                                                                                	<br>
 *     You should have received a copy of the GNU General Public License
 *     along with Unified-API.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  \author Lorenzo Persampieri, lorenzo.persampieri@studio.unibo.it
 *
 *  \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 *	\par Revision History
 *
 *  DD/MM/YY | AUTHOR          |  DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  14/11/22 | L. Persampieri  |  Initial Implementation.
 *  02/02/24 | A. Genovese     |  Added metadata support
 */


//#define UNIBOBP_IMPLEMENTATION
#ifdef UNIBOBP_IMPLEMENTATION


#include "bp_unibo_bp_conversions.h"
#include "unified_api_system_libraries.h"

al_bp_error_t al_from_unibo_bp_error(UniboBPError unibo_bp_error) {
    switch (unibo_bp_error) {
        case UniboBP_NoError:               return BP_SUCCESS;
        case UniboBPError_System:           return BP_ERRBASE;
        case UniboBPError_InvalidArgument:  return BP_EINVAL;
        case UniboBPError_MalformedEID:     return BP_EINVAL;
        case UniboBPError_ConnectionClosed: return BP_ECONNECT;
        case UniboBPError_Internal:         return BP_EINTERNAL;
        case UniboBPError_Server:           return BP_ERRBASE;
        case UniboBPError_Timeout:          return BP_ETIMEOUT;
        case UniboBPError_BundleNotFound:   return BP_ERRBASE;
    }

    return BP_SUCCESS;
}

UniboBPUserHandle al_to_unibo_bp_handle(al_types_handle al_handle) {
    return (UniboBPUserHandle) al_handle;
}
al_types_handle al_from_unibo_bp_handle(UniboBPUserHandle unibo_bp_handle) {
    return (al_types_handle) unibo_bp_handle;
}

UniboBPScheme al_to_unibo_bp_scheme(al_types_scheme al_scheme) {
    switch (al_scheme) {
        case DTN_SCHEME: return UniboBPScheme_dtn;
        case IPN_SCHEME: return UniboBPScheme_ipn;
    }
    return UniboBPScheme_unknown;
}
al_types_scheme al_from_unibo_bp_scheme(UniboBPScheme unibo_bp_scheme) {
    switch (unibo_bp_scheme) {
        case UniboBPScheme_dtn: return DTN_SCHEME;
        case UniboBPScheme_ipn: return IPN_SCHEME;
        case UniboBPScheme_imc:     break;
        case UniboBPScheme_unknown: break;
    }

    return DTN_SCHEME; /* scheme not found in Unified-API -- however Unified-API does not have an "unknown" enum value */
}

uint64_t al_to_unibo_bp_bundle_processing_control_flags(uint64_t al_flags) {
    uint64_t unibo_bp_flags = 0;
    // TODO missing in unified api : isFragment, isAdministrativeRecord
    if (al_flags & BP_DOPTS_DO_NOT_FRAGMENT)
        unibo_bp_flags |= UniboBPBundleProcessingControlFlag_mustNotBeFragmented;
    if (al_flags & BP_DOPTS_ACK_REQUESTED_BY_APPLICATION)
        unibo_bp_flags |= UniboBPBundleProcessingControlFlag_acknowledgmentRequested;
    if (al_flags & BP_DOPTS_STATUS_TIME_IN_BSR)
        unibo_bp_flags |= UniboBPBundleProcessingControlFlag_statusTimeRequested;
    if (al_flags & BP_DOPTS_RECEPTION_BSR) {
        unibo_bp_flags |= UniboBPBundleProcessingControlFlag_requestReportingBundleReception;
        /* note: dtnperf client does not set BP_DOPTS_STATUS_TIME_IN_BSR because apparently
         *       other implementations does not have this option.
         *       We set it as default here.
         */
        unibo_bp_flags |= UniboBPBundleProcessingControlFlag_statusTimeRequested;
    }
    if (al_flags & BP_DOPTS_DELIVERY_BSR) {
        unibo_bp_flags |= UniboBPBundleProcessingControlFlag_requestReportingBundleDelivery;
        /* note: dtnperf client does not set BP_DOPTS_STATUS_TIME_IN_BSR because apparently
         *       other implementations does not have this option.
         *       We set it as default here.
         */
        unibo_bp_flags |= UniboBPBundleProcessingControlFlag_statusTimeRequested;
    }
    if (al_flags & BP_DOPTS_FORWARD_BSR) {
        unibo_bp_flags |= UniboBPBundleProcessingControlFlag_requestReportingBundleForwarding;
        /* note: dtnperf client does not set BP_DOPTS_STATUS_TIME_IN_BSR because apparently
         *       other implementations does not have this option.
         *       We set it as default here.
         */
        unibo_bp_flags |= UniboBPBundleProcessingControlFlag_statusTimeRequested;
    }
    if (al_flags & BP_DOPTS_DELETION_BSR) {
        unibo_bp_flags |= UniboBPBundleProcessingControlFlag_requestReportingBundleDeletion;
        /* note: dtnperf client does not set BP_DOPTS_STATUS_TIME_IN_BSR because apparently
         *       other implementations does not have this option.
         *       We set it as default here.
         */
        unibo_bp_flags |= UniboBPBundleProcessingControlFlag_statusTimeRequested;
    }


    return unibo_bp_flags;
}
al_types_bundle_processing_control_flags al_from_unibo_bp_bundle_processing_control_flags(uint64_t unibo_bp_flags) {
    uint64_t al_flags = 0;
    // TODO not found in unified api : isFragment, isAdministrativeRecord
    if (unibo_bp_flags & UniboBPBundleProcessingControlFlag_mustNotBeFragmented)
        al_flags |= BP_DOPTS_DO_NOT_FRAGMENT;
    if (unibo_bp_flags & UniboBPBundleProcessingControlFlag_acknowledgmentRequested)
        al_flags |= BP_DOPTS_ACK_REQUESTED_BY_APPLICATION;
    if (unibo_bp_flags & UniboBPBundleProcessingControlFlag_statusTimeRequested)
        al_flags |= BP_DOPTS_STATUS_TIME_IN_BSR;
    if (unibo_bp_flags & UniboBPBundleProcessingControlFlag_requestReportingBundleReception)
        al_flags |= BP_DOPTS_RECEPTION_BSR;
    if (unibo_bp_flags & UniboBPBundleProcessingControlFlag_requestReportingBundleDelivery)
        al_flags |= BP_DOPTS_DELIVERY_BSR;
    if (unibo_bp_flags & UniboBPBundleProcessingControlFlag_requestReportingBundleForwarding)
        al_flags |= BP_DOPTS_FORWARD_BSR;
    if (unibo_bp_flags & UniboBPBundleProcessingControlFlag_requestReportingBundleDeletion)
        al_flags |= BP_DOPTS_DELETION_BSR;

    /* this is a unified-api design error (enum used as bit mask) */
    al_types_bundle_processing_control_flags al_flags_enum = (al_types_bundle_processing_control_flags) al_flags;
    return al_flags_enum;
}

uint16_t al_to_unibo_bp_ecos_flags(al_types_bundle_ecos_t *al_ecos) {
    uint16_t unibo_bp_flags = 0;
    if (al_ecos->critical) {
        unibo_bp_flags |= UniboBPECOSFlag_critical;
    }
    if (al_ecos->unreliable) {
        unibo_bp_flags |= UniboBPECOSFlag_bestEffortForward;
    } else {
        unibo_bp_flags |= UniboBPECOSFlag_reliableForward;
    }
    if (al_ecos->flow_label) {
        unibo_bp_flags |= UniboBPECOSFlag_qosTag;
    }

    return unibo_bp_flags;
}
void al_from_unibo_bp_ecos_flags(uint16_t unibo_bp_flags,
                                 al_types_bundle_ecos_t *al_ecos) {
    al_ecos->critical = unibo_bp_flags & UniboBPECOSFlag_critical;
    al_ecos->unreliable = unibo_bp_flags & UniboBPECOSFlag_bestEffortForward;
}

UniboBPECOSCardinalPriority al_to_unibo_bp_cardinal_priority(al_types_bundle_priority_enum al_priority) {
    switch (al_priority) {
        case BP_PRIORITY_BULK:        return UniboBPECOSCardinalPriority_bulk;
        case BP_PRIORITY_NORMAL:      return UniboBPECOSCardinalPriority_normal;
        case BP_PRIORITY_EXPEDITED:   return UniboBPECOSCardinalPriority_expedited;
        case BP_PRIORITY_RESERVED:    break;
    }
    return UniboBPECOSCardinalPriority_unknown;
}
al_types_bundle_priority_enum al_from_unibo_bp_cardinal_priority(UniboBPECOSCardinalPriority unibo_bp_priority) {
    switch (unibo_bp_priority) {
        case UniboBPECOSCardinalPriority_bulk:      return BP_PRIORITY_BULK;
        case UniboBPECOSCardinalPriority_normal:    return BP_PRIORITY_NORMAL;
        case UniboBPECOSCardinalPriority_expedited: return BP_PRIORITY_EXPEDITED;
        case UniboBPECOSCardinalPriority_unknown:   break;
    }

    return BP_PRIORITY_RESERVED;
}


///////////////////////////////// implementation of metadata (extensions) methods /////////////////////////////////

void al_to_unibo_bp_extensions_metadata(al_types_extension_block *extension_blocks, UniboBPMetadataType *metadata_type, uint64_t *metadata_length, char **metadata_string){
    char backup_specific_data[1000];
	strcpy (backup_specific_data, extension_blocks->block_data.block_type_specific_data);
	char *metadata_type_in_string_format = strtok(backup_specific_data, "/"); //obtain the metadata type as a string (usually "1")
    *metadata_type = atoi(metadata_type_in_string_format);

    char *metadata_string_temp = "";

    //block-type-specific data: "1/yellow"

	int total_length = (int) strlen(extension_blocks->block_data.block_type_specific_data) - 1; //length excluding the delimiter (/) --> 7
	int partial_length = (int) strlen(metadata_type_in_string_format); //partial length is the lenght of the metadata type in string format --> 1

	if(total_length != partial_length){ //It means that there is something after the metadata type in string format
		char *temp_metadata_string = strtok(NULL, "\0");
        metadata_string_temp = malloc(sizeof(temp_metadata_string)+1);
        memcpy(metadata_string_temp, temp_metadata_string, sizeof(temp_metadata_string)+1);
		strcpy (metadata_string_temp, temp_metadata_string);
	}

    uint64_t datalen = strlen(metadata_string_temp);


	if (datalen != 0) {
        *metadata_length = datalen & 0xFF;

        char *temp_metadata_string = strtok(NULL, "\0");
        *metadata_string = malloc(sizeof(temp_metadata_string)+1);
        memcpy(*metadata_string, metadata_string_temp, datalen+1);
        strcpy (*metadata_string, metadata_string_temp);
	}
	else {
        *metadata_length = 0;
        *metadata_string = "";
	}
}

void al_from_unibo_bp_extensions_metadata(ConstUniboBPInboundBundle const_unibo_bp_bundle, al_types_extension_block **extension_blocks){

    int i = 0;

    //This re-initialization of metadata_val is to avoid eventual segmentation faults.
    free(*extension_blocks);
    (*extension_blocks) = (al_types_extension_block *)malloc(sizeof(al_types_extension_block));

    (*extension_blocks)[i].block_type_code = (unsigned int) unibo_bp_inbound_bundle_metadata_get_type(const_unibo_bp_bundle);
    (*extension_blocks)[i].block_processing_control_flags=0;
    
    uint64_t metadata_type_temp = unibo_bp_inbound_bundle_metadata_get_type(const_unibo_bp_bundle);
    char metadata_type_string[100] = {0};
    sprintf(metadata_type_string, "%ld", metadata_type_temp);
    char *metadata_string = unibo_bp_inbound_bundle_metadata_get_string(const_unibo_bp_bundle);
    char block_type_data[1000];
    sprintf(block_type_data,"%s/%s", metadata_type_string, metadata_string);
    uint64_t block_type_data_len = strlen(block_type_data);
    //PRESENT IN BPv6 and in Unified API for compatibility with BPv6
    (*extension_blocks)[i].block_data.block_type_specific_data_len = block_type_data_len;


    if(block_type_data_len > 0) {
        (*extension_blocks)[i].block_data.block_type_specific_data = malloc(block_type_data_len * sizeof(char)+1);

        memcpy((*extension_blocks)[i].block_data.block_type_specific_data, block_type_data, block_type_data_len + 1);
        strcpy((*extension_blocks)[i].block_data.block_type_specific_data, block_type_data);
    }
    else {
        (*extension_blocks)[i].block_data.block_type_specific_data = NULL;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////

uint64_t al_to_unibo_bp_lifetime(al_types_timeval lifetime) {
    /* Unified-API lifetime is in seconds -- Unibo-BP (BPv7) uses milliseconds */
    return lifetime*1000;
}
al_types_timeval al_from_unibo_bp_lifetime(uint64_t lifetime) {
    return lifetime/1000;
}

UniboBPError al_to_unibo_bp_payload_reader(al_types_bundle_payload* al_payload,
                                           UniboBPBlockReader* unibo_bp_block_reader) {
    switch (al_payload->location) {
        case BP_PAYLOAD_FILE:
            return unibo_bp_block_reader_create_from_filename(al_payload->filename.filename_val,
                                                              unibo_bp_block_reader);
        case BP_PAYLOAD_MEM:
            return unibo_bp_block_reader_create_from_memory((const uint8_t *) al_payload->buf.buf_val,
                                                            unibo_bp_block_reader);
        case BP_PAYLOAD_TEMP_FILE:
            return unibo_bp_block_reader_create_from_filename(al_payload->filename.filename_val,
                                                              unibo_bp_block_reader);
    }

    return UniboBPError_InvalidArgument;
}

UniboBPError al_to_unibo_bp_payload_writer(al_types_bundle_payload* al_payload,
                                           UniboBPBlockWriter* unibo_bp_block_writer) {
    switch (al_payload->location) {
        case BP_PAYLOAD_FILE:
            return unibo_bp_block_writer_create_from_filename(al_payload->filename.filename_val,
                                                              unibo_bp_block_writer);
        case BP_PAYLOAD_MEM:
            return unibo_bp_block_writer_create_from_memory((uint8_t *) al_payload->buf.buf_val,
                                                            unibo_bp_block_writer);
        case BP_PAYLOAD_TEMP_FILE:
            return unibo_bp_block_writer_create_from_filename(al_payload->filename.filename_val,
                                                              unibo_bp_block_writer);
    }

    return UniboBPError_InvalidArgument;
}


static al_types_status_report_reason al_from_unibo_bp_status_report_reason_code(UniboBPStatusReportReasonCode unibo_bp_reason_code) {
    switch (unibo_bp_reason_code) {
        case UniboBPStatusReportReasonCode_noAdditionalInformation:             return BP_SR_REASON_NO_ADDTL_INFO;
        case UniboBPStatusReportReasonCode_lifetimeExpired:                     return BP_SR_REASON_LIFETIME_EXPIRED;
        case UniboBPStatusReportReasonCode_forwardedOverUnidirectionalLink:     return BP_SR_REASON_FORWARDED_UNIDIR_LINK;
        case UniboBPStatusReportReasonCode_transmissionCanceled:                return BP_SR_REASON_TRANSMISSION_CANCELLED;
        case UniboBPStatusReportReasonCode_depletedStorage:                     return BP_SR_REASON_DEPLETED_STORAGE;
        case UniboBPStatusReportReasonCode_destinationEndpointIDUnavailable:    return BP_SR_REASON_ENDPOINT_ID_UNINTELLIGIBLE;
        case UniboBPStatusReportReasonCode_noKnownRouteToDestinationFromHere:   return BP_SR_REASON_NO_ROUTE_TO_DEST;
        case UniboBPStatusReportReasonCode_noTimelyContactWithNextNodeOnRoute:  return BP_SR_REASON_NO_TIMELY_CONTACT;
        case UniboBPStatusReportReasonCode_blockUnintelligible:                 return BP_SR_REASON_BLOCK_UNINTELLIGIBLE;
        case UniboBPStatusReportReasonCode_hopLimitExceeded:                    break; // TODO not found in Unified API
        case UniboBPStatusReportReasonCode_trafficPared:                        break; // TODO not found in Unified API
        case UniboBPStatusReportReasonCode_blockUnsupported:                    break; // TODO not found in Unified API
        case UniboBPStatusReportReasonCode_missingSecurityOperation:            break; // TODO not found in Unified API
        case UniboBPStatusReportReasonCode_unknownSecurityOperation:            break; // TODO not found in Unified API
        case UniboBPStatusReportReasonCode_unexpectedSecurityOperation:         break; // TODO not found in Unified API
        case UniboBPStatusReportReasonCode_failedSecurityOperation:             break; // TODO not found in Unified API
        case UniboBPStatusReportReasonCode_conflictingSecurityOperation:        break; // TODO not found in Unified API
        case UniboBPStatusReportReasonCode_unknown:                             break;
    }

    return BP_SR_REASON_NO_ADDTL_INFO; /* status report reason code not found in Unified-API */
}

static al_bp_error_t al_from_unibo_bp_status_report(al_types_bundle_payload* payload, ConstUniboBPAdministrativeRecord const_unibo_bp_administrative_record) {
    UniboBPError unibo_bp_error;
    ConstUniboBPStatusReport const_unibo_status_report = unibo_bp_const_cast_status_report(const_unibo_bp_administrative_record);

    if (!payload->status_report) {
        payload->status_report = (al_types_bundle_status_report*) malloc(sizeof(al_types_bundle_status_report));
        if (!payload->status_report) {
            return BP_ENULLPNTR;
        }
    }

    ConstUniboBPEID unibo_bp_source_node_id = unibo_bp_status_report_get_source_node_id(const_unibo_status_report);
    unibo_bp_error = unibo_bp_eid_to_string(unibo_bp_source_node_id, payload->status_report->bundle_x_id.source.uri, sizeof(payload->status_report->bundle_x_id.source));
    if (unibo_bp_error != UniboBP_NoError) {
        return al_from_unibo_bp_error(unibo_bp_error);
    }

    payload->status_report->bundle_x_id.creation_ts.time = unibo_bp_status_report_get_bundle_creation_time(const_unibo_status_report);
    payload->status_report->bundle_x_id.creation_ts.seqno = unibo_bp_status_report_get_bundle_sequence_number(const_unibo_status_report);

    payload->status_report->bundle_x_id.frag_offset = unibo_bp_status_report_get_bundle_fragment_offset(const_unibo_status_report);
    payload->status_report->bundle_x_id.frag_length = unibo_bp_status_report_get_bundle_fragment_length(const_unibo_status_report);

    UniboBPStatusReportReasonCode unibo_bp_reason_code = unibo_bp_status_report_get_reason_code(const_unibo_status_report);
    payload->status_report->reason = al_from_unibo_bp_status_report_reason_code(unibo_bp_reason_code);

    payload->status_report->flags = (al_types_status_report_flags) 0;
    bool sr_received = unibo_bp_status_report_get_status_information(const_unibo_status_report, UniboBPStatusInformation_bundleReceived);
    payload->status_report->flags |= (al_types_status_report_flags) ((sr_received) ? BP_STATUS_RECEIVED : 0);
    payload->status_report->reception_ts = unibo_bp_status_report_get_report_status_time(const_unibo_status_report, UniboBPStatusInformation_bundleReceived);

    bool sr_forwarded = unibo_bp_status_report_get_status_information(const_unibo_status_report, UniboBPStatusInformation_bundleForwarded);
    payload->status_report->flags |= (al_types_status_report_flags) ((sr_forwarded) ? BP_STATUS_FORWARDED : 0);
    payload->status_report->forwarding_ts = unibo_bp_status_report_get_report_status_time(const_unibo_status_report, UniboBPStatusInformation_bundleForwarded);

    bool sr_delivered = unibo_bp_status_report_get_status_information(const_unibo_status_report, UniboBPStatusInformation_bundleDelivered);
    payload->status_report->flags |= (al_types_status_report_flags) ((sr_delivered) ? BP_STATUS_DELIVERED : 0);
    payload->status_report->delivery_ts = unibo_bp_status_report_get_report_status_time(const_unibo_status_report, UniboBPStatusInformation_bundleDelivered);

    bool sr_deleted = unibo_bp_status_report_get_status_information(const_unibo_status_report, UniboBPStatusInformation_bundleDeleted);
    payload->status_report->flags |= (al_types_status_report_flags) ((sr_deleted) ? BP_STATUS_DELETED : 0);
    payload->status_report->deletion_ts = unibo_bp_status_report_get_report_status_time(const_unibo_status_report, UniboBPStatusInformation_bundleDeleted);

    return al_from_unibo_bp_error(UniboBP_NoError);
}

al_bp_error_t al_from_unibo_bp_administrative_record(al_types_bundle_payload* payload, UniboBPBlockReader unibo_bp_payload_reader) {
    UniboBPError unibo_bp_error;
    UniboBPAdministrativeRecord unibo_bp_administrative_record;

    unibo_bp_error = unibo_bp_administrative_record_create(unibo_bp_payload_reader, &unibo_bp_administrative_record);
    if (unibo_bp_error != UniboBP_NoError) {
        return al_from_unibo_bp_error(unibo_bp_error);
    }

    ConstUniboBPAdministrativeRecord const_unibo_bp_administrative_record = unibo_bp_const_cast_administrative_record(unibo_bp_administrative_record);
    UniboBPAdministrativeRecordType unibo_bp_administrative_record_type = unibo_bp_administrative_record_get_record_type(const_unibo_bp_administrative_record);

    al_bp_error_t al_bp_error = BP_SUCCESS;
    switch (unibo_bp_administrative_record_type) {
        case UniboBPAdministrativeRecordType_unknown:
            break;
        case UniboBPAdministrativeRecordType_statusReport:
            al_bp_error = al_from_unibo_bp_status_report(payload, const_unibo_bp_administrative_record);
    }

    unibo_bp_administrative_record_destroy(&unibo_bp_administrative_record);
    return al_bp_error;
}

#endif
