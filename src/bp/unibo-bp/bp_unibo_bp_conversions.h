/** \file bp_unibo_bp_conversions.h
 *
 *  \brief  This file contains the prototypes of the functions implemented in the homonymous .c file.
 *
 *  \par Copyright
 *  	Copyright (c) 2022, Alma Mater Studiorum, University of Bologna. All rights reserved.
 *
 *  \par License
 *     This file is part of Unified-API.                                            	<br>
 *                                                                                	<br>
 *     Unified-API is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.                                        	<br>
 *     Unified-API is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.                               	<br>
 *                                                                                	<br>
 *     You should have received a copy of the GNU General Public License
 *     along with Unified-API.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  \author Lorenzo Persampieri, lorenzo.persampieri@studio.unibo.it
 *
 *  \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 *	\par Revision History
 *
 *  DD/MM/YY | AUTHOR          |  DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  14/11/22 | L. Persampieri  |  Initial Implementation.
 *  02/02/24 | A. Genovese     |  Added metadata support
 */

#ifndef UNIFIED_API_UNIBO_BP_BP_UNIBO_BP_CONVERSIONS_H
#define UNIFIED_API_UNIBO_BP_BP_UNIBO_BP_CONVERSIONS_H

#include <Unibo-BP/bp.h>
#include "al_types.h"
#include "al_bp.h"

#ifdef __cplusplus
extern "C" {
#endif

al_bp_error_t al_from_unibo_bp_error(UniboBPError unibo_bp_error);

UniboBPUserHandle al_to_unibo_bp_handle(al_types_handle al_handle);
al_types_handle al_from_unibo_bp_handle(UniboBPUserHandle unibo_bp_handle);

UniboBPScheme al_to_unibo_bp_scheme(al_types_scheme al_scheme);
al_types_scheme al_from_unibo_bp_scheme(UniboBPScheme unibo_bp_scheme);

uint64_t al_to_unibo_bp_bundle_processing_control_flags(uint64_t al_flags);
al_types_bundle_processing_control_flags al_from_unibo_bp_bundle_processing_control_flags(uint64_t unibo_bp_flags);

uint16_t al_to_unibo_bp_ecos_flags(al_types_bundle_ecos_t *al_ecos);
void al_from_unibo_bp_ecos_flags(uint16_t unibo_bp_flags,
                                 al_types_bundle_ecos_t *al_ecos);

UniboBPECOSCardinalPriority al_to_unibo_bp_cardinal_priority(al_types_bundle_priority_enum al_priority);
al_types_bundle_priority_enum al_from_unibo_bp_cardinal_priority(UniboBPECOSCardinalPriority unibo_bp_priority);

void al_to_unibo_bp_extensions_metadata(al_types_extension_block *extension_blocks,
                                        UniboBPMetadataType *metadata_type, uint64_t *metadata_length, char **metadata_string);
void al_from_unibo_bp_extensions_metadata(ConstUniboBPInboundBundle const_unibo_bp_bundle, al_types_extension_block **extension_blocks);

uint64_t al_to_unibo_bp_lifetime(al_types_timeval lifetime);
al_types_timeval al_from_unibo_bp_lifetime(uint64_t lifetime);

UniboBPError al_to_unibo_bp_payload_reader(al_types_bundle_payload* al_payload,
                                           UniboBPBlockReader* unibo_bp_block_reader);

UniboBPError al_to_unibo_bp_payload_writer(al_types_bundle_payload* al_payload,
                                           UniboBPBlockWriter* unibo_bp_block_writer);

al_bp_error_t al_from_unibo_bp_administrative_record(al_types_bundle_payload* payload, UniboBPBlockReader unibo_bp_payload_reader);

#ifdef __cplusplus
}
#endif

#endif //UNIFIED_API_UNIBO_BP_BP_UNIBO_BP_CONVERSIONS_H
