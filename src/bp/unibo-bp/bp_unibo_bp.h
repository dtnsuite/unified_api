/** \file bp_unibo_bp.h
 *
 *  \brief  This file contains the prototypes of the functions implemented in the homonymous .c file.
 *
 *  \par Copyright
 *  	Copyright (c) 2022, Alma Mater Studiorum, University of Bologna. All rights reserved.
 *
 *  \par License
 *     This file is part of Unified-API.                                            	<br>
 *                                                                                	<br>
 *     Unified-API is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.                                        	<br>
 *     Unified-API is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.                               	<br>
 *                                                                                	<br>
 *     You should have received a copy of the GNU General Public License
 *     along with Unified-API.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  \author Lorenzo Persampieri, lorenzo.persampieri@studio.unibo.it
 *
 *  \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 *	\par Revision History
 *
 *  DD/MM/YY | AUTHOR          |  DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  14/11/22 | L. Persampieri  |  Initial Implementation.
 */


#ifndef bp_unibo_bp_H
#define bp_unibo_bp_H

#include <stdbool.h>
#include "al_types.h"
#include "al_bp.h"

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Check if Unibo-BP is running.
 */
bool bp_unibo_bp_is_running();

al_bp_error_t bp_unibo_bp_open(al_types_handle* handle);

al_bp_error_t bp_unibo_bp_open_with_IP(char * daemon_api_IP,
                                       int daemon_api_port,
                                       al_types_handle * handle);

al_bp_error_t bp_unibo_bp_errno(al_types_handle handle);


al_bp_error_t bp_unibo_bp_build_local_eid(al_types_handle handle,
                                          al_types_endpoint_id* local_eid,
                                          const char* service_tag,
                                          al_types_scheme type);


al_bp_error_t bp_unibo_bp_register(al_types_handle handle,
                                   al_types_reg_info* reginfo);

al_bp_error_t bp_unibo_bp_find_registration(al_types_handle handle,
                                            al_types_endpoint_id * eid);

al_bp_error_t bp_unibo_bp_unregister(al_types_handle handle);

al_bp_error_t bp_unibo_bp_send(al_types_handle handle,
                               al_types_bundle_spec* spec,
                               al_types_bundle_payload* payload);

al_bp_error_t bp_unibo_bp_recv(al_types_handle handle,
                               al_types_bundle_spec* spec,
                               al_types_bundle_payload_location location,
                               al_types_bundle_payload* payload,
                               al_types_timeout timeout);

al_bp_error_t bp_unibo_bp_close(al_types_handle handle);

void bp_unibo_bp_copy_eid(al_types_endpoint_id* dst, al_types_endpoint_id* src);

al_bp_error_t bp_unibo_bp_parse_eid_string(al_types_endpoint_id* eid,
                                           const char* str);

al_bp_error_t bp_unibo_bp_set_payload(al_types_bundle_payload* payload,
                                      al_types_bundle_payload_location location,
                                      char* val,
                                      int len);

void bp_unibo_bp_free_payload(al_types_bundle_payload* payload);

void bp_unibo_bp_free_extensions(al_types_bundle_spec* spec);

#ifdef __cplusplus
}
#endif

#endif /* bp_unibo_bp_H */