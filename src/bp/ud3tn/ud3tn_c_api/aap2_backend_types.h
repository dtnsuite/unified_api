/** \file ud3tn_c_api_types.h
 *
 * \brief This file contains the ud3tn C API types
 *
  * \brief This file contains the functions that implement a C API on top of the
 * ud3tn AAP2. Destined to be moved to ud3tn official code.
 *
 * \copyright (c) 2024 Alma Mater Studiorum, University of Bologna.
 *
 * \par License
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 * \author Fabio Colonna, fabio.colonna3@studio.unibo.it
 * \author Beatrice Barbieri, beatrice.barbieri7@studio.unibo.it
 */

#pragma once

#include "aap2.pb.h"
#include "bundle.h"
///////////////////////////////////////////////////////////
#include <stddef.h>
#include <stdint.h>
#include <pb_decode.h>
#include <pb_encode.h>
///////////////////////////////////////////////////////////

#define UD3TN_NO_TIMEOUT           -1
#define UD3TN_TYPES_MAX_EID_LENGTH 256

/**
 * @brief Environment variable used in aap_bridge.c when a UNIX socket connection
 * is requested from the upper layer to obtain the path of the socket on which
 * the AA daemon is listening.
 *
 * @note If not found, open() will fail.
 *
 */
#define AAP2_UNIX_SOCKPATH "UD3TN_SOCKET"

/**
 * @brief Used to specify the initial receive buffer capacity in bytes
 * @note Only used in recv_mesg_aap()
 */
#define RECV_BUF_INITIAL_CAP_BYTES 10 * 1000// 10 KB

/**
 * @brief Used to specify the resize factor used when remalloc-ing the recv
 * buffer
 * @note Only used in recv_mesg_aap()
 */
#define RECV_BUF_RESIZE_FACTOR 2

// Used internally
struct write_socket_param {
    int socket_fd;
    int errno_;
};

// EXISTING UD3TN TYPES

/**
 * @note BB: ClientInfo contains all relevant information regarding a connection. The connections are made to be bidirectional
 * and thus require two separate sockets and input streams.
 */

typedef struct {
    int input_socket;
    int output_socket;
    pb_istream_t output_socket_in;
    pb_istream_t input_socket_in;
    char              myEID[UD3TN_TYPES_MAX_EID_LENGTH];
    char              nodeID[UD3TN_TYPES_MAX_EID_LENGTH];
} UD3TNClientInfo;

typedef UD3TNClientInfo *ud3tn_handle_t;

typedef struct {
    int one;
    int two;
} ud3tn_sockets;

typedef struct {
    char uri[UD3TN_TYPES_MAX_EID_LENGTH];
} ud3tn_endpoint_id_t;

typedef uint64_t ud3tn_timeval_t;
typedef uint32_t ud3tn_reg_token_t;
typedef uint32_t ud3tn_reg_id_t;

typedef int ud3tn_bundle_payload_location_t; // Only memory

// FColonna: added timestamp & seqno support through new types.
typedef struct {
    uint64_t time;
    uint64_t seqno;
} ud3tn_creation_timestamp_t;

/**
 * @note BB:
 * there's still several separate structs, one to represent the payload and
 * others for additional data
 * protocol_version has not been added because it's currently
 * not required anywhere
 */
typedef struct ud3tn_bundle_spec_t {
	enum bundle_proc_flags proc_flags;
    ud3tn_endpoint_id_t        source;
    ud3tn_endpoint_id_t        dest;
    ud3tn_creation_timestamp_t creation_ts;
   	bool bdm_auth_validated;
} ud3tn_bundle_spec_t;

// Needed by status_report_t
typedef struct {
    ud3tn_endpoint_id_t        source;
    ud3tn_creation_timestamp_t creation_ts;
    uint32_t                   frag_offset;
    uint32_t                   frag_length;
} ud3tn_bundle_id_t;

// Defined because needed by status_report_t by not yet implemented
typedef void *ud3tn_status_report_reason_t;
typedef void *ud3tn_status_report_flags_t;

// CCaini brand new structure; built to remove a dependency on al_types; to be
// tested
typedef struct ud3tn_bundle_status_report_t {
    ud3tn_bundle_id_t            bundle_x_id;
    ud3tn_status_report_reason_t reason;
    ud3tn_status_report_flags_t  flags;
    uint64_t                     reception_ts;
    uint64_t                     custody_ts;
    uint64_t                     forwarding_ts;
    uint64_t                     delivery_ts;
    uint64_t                     deletion_ts;
    uint64_t                     ack_by_app_ts;
} ud3tn_bundle_status_report_t;

typedef struct ud3tn_bundle_payload_t {
    struct {
        uint32_t buf_len;
        char    *buf_val;
    } buf;
    // CCaini the dependency on al_types had to be removed
    //	al_bundle_status_report  *status_report;
    ud3tn_bundle_status_report_t *status_report;
} ud3tn_bundle_payload_t;

/**
 * @note BB: this struct contains all information that a client may require to configure a connection.
 */
typedef struct {
    ud3tn_endpoint_id_t endpoint;
    uint32_t            flags;
    char *secret;
    bool is_subscriber;
    aap2_AuthType auth_type;
} ud3tn_reg_info_t;

typedef enum
{
    UD3TN_SUCCESS,
    UD3TN_EINVAL,
    UD3TN_ECONNECT,
    UD3TN_ETIMEOUT,
    UD3TN_ESIZE,
    UD3TN_ENOTFOUND,
    UD3TN_EINTERNAL,
    UD3TN_EBUSY,
    UD3TN_ENOSPACE,
    #ifndef UNIFIED_API_FUNCTION_WORKING_WHEN_AAP2
    UD3TN_ENOTIMPL,
    #endif // UNIFIED_API_FUNCTION_WORKING_WHEN_AAP2

    /*
        FColonna: now the C interface propagates new errors to the upper layers,
       so I added them here. I also added them in the bp_ud3tn_error() function
       in the bp_ud3tn.c file, so that they can be converted to the respective
       Unified AP error types and propagated.
    */
    UD3TN_ESEND,
    UD3TN_ERECV,
    UD3TN_EOPEN,
    UD3TN_EREG,
    UD3TN_ENULLPNTR
} ud3tn_error_t;

typedef enum
{
    UD3TN_IPN_SCHEME = 0,
    UD3TN_DTN_SCHEME,
} ud3tn_scheme_t;

/* The following types have not been implemented yet:
typedef void *ud3tn_bundle_priority_t;
typedef void *ud3tn_reg_flags_t;
typedef void *ud3tn_bundle_delivery_opts_t; */
