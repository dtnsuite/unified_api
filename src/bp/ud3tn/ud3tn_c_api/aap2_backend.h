/** \file ud3tn_c_api.h
 *
 * \brief This file contains the prototypes of the functions that implement a C
 * API on top of the ud3tn AAP
 * \brief This file contains the functions that implement a C API on top of the
 * ud3tn AAP2. Destined to be moved to ud3tn official code.
 *
 * \copyright (c) 2024 Alma Mater Studiorum, University of Bologna.
 *
 * \par License
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * \author Fabio Colonna, fabio.colonna3@studio.unibo.it
 * \author Beatrice Barbieri, beatrice.barbieri7@studio.unibo.it
 */

#pragma once

// FColonna: this API is not compiled if UD3TN_IMPLEMENTATION is not defined
#ifdef UD3TN_IMPLEMENTATION

#ifdef HAVE_CONFIG_H
#include <dtn-config.h>
#endif

#include "aap2_backend_types.h"

/**
 * @brief Closes the connection with the AAP
 *
 * @param handle [INPUT] Structure that defines a connection with the AAP, that
 * is about to end
 * @return ud3tn_error_t
 */
ud3tn_error_t
ud3tn_close(ud3tn_handle_t handle);

/**
 * @brief Creates a UNIX socket that is used to communicate with the AAP
 *
 * @param out_handle [OUTPUT] Structure that defines a connection with the AAP.
 * @return ud3tn_error_t
 */
ud3tn_error_t
ud3tn_open(ud3tn_handle_t *out_handle, char* sock_path);

/**
 * @brief Creates a INET socket that is used to communicate with the AAP
 *
 * @param addr [INPUT] Address at which the AAP is listening
 * @param port [INPUT] Port at which the AAP is listening
 * @param out_handle [OUTPUT] Structure that defines a connection with the AAP
 * @return ud3tn_error_t
 */
ud3tn_error_t
ud3tn_open_with_ip(const char *const addr, int port, ud3tn_handle_t *out_handle);

/**
 * @brief Allows an application to register an EID to the AAP
 *
 * @param handle [INPUT] Structure that defines a connection with the AAP
 * @param reg_info [INPUT] Registration information
 * @param out_reg_id
 * @return ud3tn_error_t
 */
ud3tn_error_t
ud3tn_register(ud3tn_handle_t handle, ud3tn_reg_info_t *const reg_info);

/**
 * @brief Sends the bundle taken as input to the AAP
 *
 * @param handle [INPUT] Structure that defines a connection with the AAP
 * @param reg_id
 * @param spec [INPUT/OUTPUT] Bundle specifications (source & destination EIDs).
 *              Its timestamp & sequence number will be set after the function returns.
 * @param payload [INPUT] Data to be sent
 * @return ud3tn_error_t
 */
ud3tn_error_t
ud3tn_send(ud3tn_handle_t handle, ud3tn_bundle_spec_t *const spec,
           ud3tn_bundle_payload_t *const payload);

/**
 * @brief Awaits for a bundle to be received from the AAP, until a timeout (if
 * present) expires
 *
 * @param handle Structure that defines a connection with the AAP
 * @param out_spec [OUTPUT] It will contain the source EID of the received
 * bundle
 * @param out_payload [OUTPUT] Data received from AAP
 * @param timeout [INPUT] Timeout at which to stop waiting for a bundle
 * @return ud3tn_error_t
 */
ud3tn_error_t
ud3tn_recv(ud3tn_handle_t handle, ud3tn_bundle_spec_t *out_spec,
           ud3tn_bundle_payload_t *out_payload, ud3tn_timeval_t timeout);

/**
 * @brief Builds the EID of the node
 *
 * @param handle [INPUT] Structure that defines a connection with the AAP
 * @param out_local_eid [OUTPUT] EID built
 * @param service_tag [INPUT] Service tag
 * @param eid_scheme [INPUT] EID Scheme (dtn or ipn)
 * @return ud3tn_error_t UD3TN_SUCCESS or UD3TN_EINVAL if the EID scheme of the
 * node does not match eid_scheme
 */
ud3tn_error_t
ud3tn_build_local_eid(ud3tn_handle_t handle, ud3tn_endpoint_id_t *out_local_eid,
                      const char *const service_tag, ud3tn_scheme_t eid_scheme);

/**
 * @brief Copies the source EID of a bundle into the destination EID
 *
 * @param dst [OUTPUT] Destination EID
 * @param src [INPUT] Source EID
 * @return ud3tn_error_t
 */
ud3tn_error_t
ud3tn_copy_eid(ud3tn_endpoint_id_t *dst, ud3tn_endpoint_id_t *const src);

/**
 * @brief Parses a string into an EID
 *
 * @param out_eid [OUTPUT] EID
 * @param str [INPUT] String to be parsed
 * @return ud3tn_error_t
 */
ud3tn_error_t
ud3tn_parse_eid_string(ud3tn_endpoint_id_t *out_eid, const char *const str);

/**
 * @brief Sets the payload of a bundle
 *
 * @param out_payload [OUTPUT] Payload of the bundle
 * @param buf [INPUT] Data to be set as payload
 * @param buf_length [INPUT] Length of the data to be set as payload
 * @return ud3tn_error_t
 */
ud3tn_error_t
ud3tn_set_payload(ud3tn_bundle_payload_t *out_payload, const uint8_t *const buf,
                  uint32_t buf_length);

/**
 * @brief Frees the memory allocated for the payload of a bundle
 *
 * @param payload [INPUT] Payload of the bundle that is about to be freed
 */
void
ud3tn_free_payload(ud3tn_bundle_payload_t *const payload);

/**
 * @brief Pings the AAP and waits for pong
 *
 * @param handle [INPUT] Structure that defines a connection with the AAP
 * @return ud3tn_error_t
 */
ud3tn_error_t
ud3tn_ping_aap(ud3tn_handle_t handle);

#endif// UD3TN_IMPLEMENTATION
