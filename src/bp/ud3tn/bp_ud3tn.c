/** \file bp_ud3tn.c
 *
 * \brief This file contains the functions interfacing Unified-API with ud3tn C
 * API.
 *
 * \copyright (c) 2024 Alma Mater Studiorum, University of Bologna.
 * All rights reserved.
 *
 *  \par License
 *  This file is part of Unified-API. Unified-API is free software:
 *  you can redistribute it and/or modify it under the terms of the
 *  GNU General Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later version.
 *  Unified-API is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 *  or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with Unified-API.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  \author Fabio Colonna, fabio.colonna3@studio.unibo.it (minor changes)
 */

#include "bp_ud3tn.h"

#define UNUSED __attribute__((unused))

bool bp_ud3tn_is_running()
{
    const char *find_ud3tn = "ps ax | grep -w ud3tn | grep -v grep > /dev/null";
    return !system(find_ud3tn);
}

#ifdef UD3TN_IMPLEMENTATION

#include <netdb.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>

#include "bp_ud3tn_conversions.h"
#include "aap2_backend.h"
#include "aap2_backend_types.h"

// clang-format off

al_bp_error_t
bp_ud3tn_error(int code)
{
    switch (code) {
        case UD3TN_SUCCESS: 	return BP_SUCCESS;
        case UD3TN_EINVAL: 		return BP_EINVAL;
        case UD3TN_ECONNECT: 	return BP_ECONNECT;
        case UD3TN_ETIMEOUT: 	return BP_ETIMEOUT;
        case UD3TN_ESIZE:		return BP_ESIZE;
        case UD3TN_ENOTFOUND: 	return BP_ENOTFOUND;
        case UD3TN_EINTERNAL: 	return BP_EINTERNAL;
        case UD3TN_EBUSY: 		return BP_EBUSY;
        case UD3TN_ENOSPACE: 	return BP_ENOSPACE;

        // FColonna: added this new errors
        case UD3TN_ESEND: 		return BP_ESEND;
        case UD3TN_ERECV: 		return BP_ERECV;
        case UD3TN_EOPEN: 		return BP_EOPEN;
        case UD3TN_EREG: 		return BP_EREG;
        case UD3TN_ENULLPNTR: 	return BP_ENULLPNTR;
        default: 				return -1;
    }
}

// clang-format on

al_bp_error_t
bp_ud3tn_open(al_types_handle *out_handle)
{
    ud3tn_handle_t u_handle = bp_to_ud3tn_handle(*out_handle);

    //gathering socket address from env variable here. to be changed in future implementations
    //default value is ud3tn default socket value
    char *sock_path;
    if ((sock_path = getenv(AAP2_UNIX_SOCKPATH)) == NULL) {
        sock_path="./ud3tn.aap2.socket";
    }
    /////////////////////////////

    al_bp_error_t err = bp_ud3tn_error(ud3tn_open(&u_handle, sock_path));

    *out_handle = bp_from_ud3tn_handle(u_handle);
    return err;
}

al_bp_error_t
bp_ud3tn_open_with_IP(const char *const ip, int port, al_types_handle *out_handle)
{
    ud3tn_handle_t u_handle = bp_to_ud3tn_handle(*out_handle);
    al_bp_error_t err = bp_ud3tn_error(ud3tn_open_with_ip(ip, port, &u_handle));

    *out_handle = bp_from_ud3tn_handle(u_handle);
    return err;
}

al_bp_error_t
bp_ud3tn_build_local_eid(al_types_handle handle, al_types_endpoint_id *out_local_eid,
    const char *const service_tag, al_types_scheme eid_scheme)
{
    ud3tn_handle_t u_handle = bp_to_ud3tn_handle(handle);
    ud3tn_endpoint_id_t u_local_eid = bp_to_ud3tn_endpoint_id(out_local_eid);

    // FColonna: just like UniboBP, it's better to convert the EID scheme
    // explicitly, instead of just passing al_types_scheme to ud3tn_scheme_t.
    // This to avoid possible errors in the future (e.g. if the two enums are
    // not kept in sync).
    ud3tn_scheme_t u_scheme = bp_to_ud3tn_scheme(eid_scheme);

    // Error checking for u_scheme is done in the ud3tn_build_local_eid function
    // (switch)
    al_bp_error_t err = bp_ud3tn_error(ud3tn_build_local_eid(u_handle, &u_local_eid, service_tag, u_scheme));

    handle = bp_from_ud3tn_handle(u_handle);
    *out_local_eid = bp_from_ud3tn_endpoint_id(&u_local_eid);
    return err;
}

al_bp_error_t
bp_ud3tn_register(al_types_handle handle, al_types_reg_info *const reg_info,
    al_types_reg_id *out_reg_id)
{
    ud3tn_handle_t u_handle = bp_to_ud3tn_handle(handle);
    ud3tn_reg_info_t u_reginfo = bp_to_ud3tn_reg_info(reg_info);

    al_bp_error_t err = bp_ud3tn_error(ud3tn_register(u_handle, &u_reginfo));

    handle = bp_from_ud3tn_handle(u_handle);
    *reg_info = bp_from_ud3tn_reg_info(&u_reginfo);
    
    return err;
}

al_bp_error_t
bp_ud3tn_close(al_types_handle handle)
{
    ud3tn_handle_t u_handle = bp_to_ud3tn_handle(handle);
    al_bp_error_t err = bp_ud3tn_error(ud3tn_close(u_handle));

    handle = bp_from_ud3tn_handle(u_handle);
    return err;
}

al_bp_error_t
bp_ud3tn_send(al_types_handle handle, al_types_reg_id regid, al_types_bundle_spec *const spec,
    al_types_bundle_payload *const payload)
{
    ud3tn_handle_t u_handle = bp_to_ud3tn_handle(handle);
    ud3tn_bundle_spec_t u_spec = bp_to_ud3tn_bundle_spec(spec);
    ud3tn_bundle_payload_t u_payload = bp_to_ud3tn_bundle_payload(payload);

    al_bp_error_t err = bp_ud3tn_error(ud3tn_send(u_handle, &u_spec, &u_payload));

    handle = bp_from_ud3tn_handle(u_handle);
    *spec = bp_from_ud3tn_bundle_spec(&u_spec);
    *payload = bp_from_ud3tn_bundle_payload(&u_payload);
    return err;
}

al_bp_error_t
bp_ud3tn_recv(al_types_handle handle, al_types_bundle_spec *out_spec,
    al_types_bundle_payload_location location, al_types_bundle_payload *out_payload,
    al_types_timeout timeout)
{
    ud3tn_handle_t u_handle = bp_to_ud3tn_handle(handle);
    ud3tn_bundle_spec_t u_spec = bp_to_ud3tn_bundle_spec(out_spec);
    ud3tn_bundle_payload_location_t u_location = bp_to_ud3tn_bundle_payload_location(location);
    ud3tn_bundle_payload_t u_payload = bp_to_ud3tn_bundle_payload(out_payload);
    ud3tn_timeval_t u_timeout = bp_to_ud3tn_timeout(timeout);

    al_bp_error_t err = bp_ud3tn_error(ud3tn_recv(u_handle, &u_spec, &u_payload, u_timeout));

    handle = bp_from_ud3tn_handle(u_handle);
    *out_spec = bp_from_ud3tn_bundle_spec(&u_spec);
    location = bp_from_ud3tn_bundle_payload_location(u_location);
    *out_payload = bp_from_ud3tn_bundle_payload(&u_payload);
    timeout = bp_from_ud3tn_timeout(u_timeout);
    return err;
}

void bp_ud3tn_copy_eid(al_types_endpoint_id *dst, al_types_endpoint_id *const src)
{
    ud3tn_endpoint_id_t u_dst = bp_to_ud3tn_endpoint_id(dst);
    ud3tn_endpoint_id_t u_src = bp_to_ud3tn_endpoint_id(src);
    ud3tn_copy_eid(&u_dst, &u_src);
    *dst = bp_from_ud3tn_endpoint_id(&u_dst);
    *src = bp_from_ud3tn_endpoint_id(&u_src);
}

al_bp_error_t
bp_ud3tn_parse_eid_string(al_types_endpoint_id *out_eid, const char *const str)
{
    ud3tn_endpoint_id_t u_eid = bp_to_ud3tn_endpoint_id(out_eid);
    al_bp_error_t err = bp_ud3tn_error(ud3tn_parse_eid_string(&u_eid, str));

    *out_eid = bp_from_ud3tn_endpoint_id(&u_eid);
    return err;
}

al_bp_error_t
bp_ud3tn_set_payload(al_types_bundle_payload *const payload,
    al_types_bundle_payload_location location, const char *const buf,
    uint32_t buf_length)
{
    memset(payload, 0, sizeof(al_types_bundle_payload));

    payload->location = location;
    switch (location) {
        case BP_PAYLOAD_MEM:
            payload->buf.buf_len = buf_length;
            payload->buf.buf_val = (char *) buf;
            break;
        case BP_PAYLOAD_FILE:
        case BP_PAYLOAD_TEMP_FILE:
            payload->filename.filename_len = buf_length;
            payload->filename.filename_val = (char *) buf;
            break;
    }

    return BP_SUCCESS;
}

void bp_ud3tn_free_extensions(al_types_bundle_spec *const spec)
{
    for (int i = 0; i < spec->extensions.extension_number; i++) {
        al_types_extension_block *block = &(spec->extensions.extension_blocks[i]);
        if (block->block_data.block_type_specific_data) {
            free(block->block_data.block_type_specific_data);
            block->block_data.block_type_specific_data = NULL;
        }
    }

    if (spec->extensions.extension_blocks != NULL) {
        free(spec->extensions.extension_blocks);
        spec->extensions.extension_blocks = NULL;
    }

    spec->extensions.extension_number = 0;
}

void bp_ud3tn_free_payload(al_types_bundle_payload *const payload)
{
    if (payload->status_report != NULL) {
        free(payload->status_report);
        payload->status_report = NULL;
    }

    ud3tn_bundle_payload_t u_payload = bp_to_ud3tn_bundle_payload(payload);
    ud3tn_free_payload(&u_payload);
    *payload = bp_from_ud3tn_bundle_payload(&u_payload);
}

/**
 * @note NOT IMPLEMENTED
 */
al_bp_error_t
bp_ud3tn_find_registration(UNUSED al_types_handle handle,
    UNUSED al_types_endpoint_id *const eid, UNUSED al_types_reg_id *regid)
{
    return BP_ENOTIMPL;
}

/**
 * @note NOT IMPLEMENTED
 */
al_bp_error_t
bp_ud3tn_unregister(UNUSED al_types_handle handle, UNUSED al_types_reg_id regid)
{
    return BP_ENOTIMPL;
}

/**
 * @note NOT IMPLEMENTED
 */
al_bp_error_t
bp_ud3tn_errno(UNUSED al_types_handle handle)
{
    return BP_ENOTIMPL;
}

/*
al_bp_error_t bp_ud3tn_set_payload(al_types_bundle_payload* payload,
                                                        al_types_bundle_payload_location location,
                                                        char* val, int len)
{
        int result = -1;
        payload->location = location;
        ud3tn_bundle_payload_t ud3tn_payload = bp_to_ud3tn_bundle_payload(*payload);
        //ud3tn_bundle_payload_location_t ud3tn_location = bp_to_ud3tn_bundle_payload_location(location);
        //ud3tn_bundle_payload_location_t ud3tn_location = BP_PAYLOAD_MEM;

        if ( 0 ) {
                ud3tn_payload.buf.buf_val = (char *) malloc(len);
                ud3tn_payload.buf.buf_len = len;
                result = ud3tn_set_payload(& ud3tn_payload, val, len);
        } else {
                ud3tn_payload.buf.buf_val = payload->buf.buf_val;
                ud3tn_payload.buf.buf_len = payload->buf.buf_len;
                result = BP_SUCCESS;
        }
        *payload = bp_from_ud3tn_bundle_payload(ud3tn_payload);
        return get_al_error(result);
}
*/

/*
 * if Unified API is not compiled for microD3TN, the dummy functions below are compiled instead of real functions above
 * (to avoid compilation errors).
 */

#else

#include "../src/al/types/al_types.h"

al_bp_error_t
bp_ud3tn_open(UNUSED al_types_handle *out_handle)
{
    return BP_ENOTIMPL;
}

al_bp_error_t
bp_ud3tn_open_with_IP(UNUSED const char *const ip, UNUSED int port, UNUSED al_types_handle *out_handle)
{
    return BP_ENOTIMPL;
}

al_bp_error_t
bp_ud3tn_build_local_eid(UNUSED al_types_handle handle, UNUSED al_types_endpoint_id *out_local_eid,
    UNUSED const char *const service_tag, UNUSED al_types_scheme eid_scheme)
{
    return BP_ENOTIMPL;
}

al_bp_error_t
bp_ud3tn_register(UNUSED al_types_handle handle, UNUSED al_types_reg_info *const reg_info,
    UNUSED al_types_reg_id *out_reg_id)
{
    return BP_ENOTIMPL;
}

al_bp_error_t
bp_ud3tn_close(UNUSED al_types_handle handle)
{
    return BP_ENOTIMPL;
}

al_bp_error_t
bp_ud3tn_send(UNUSED al_types_handle handle, UNUSED al_types_reg_id regid,
    UNUSED al_types_bundle_spec *const spec, UNUSED al_types_bundle_payload *const payload)
{
    return BP_ENOTIMPL;
}

al_bp_error_t
bp_ud3tn_recv(UNUSED al_types_handle handle, UNUSED al_types_bundle_spec *spec,
    UNUSED al_types_bundle_payload_location location, UNUSED al_types_bundle_payload *payload,
    UNUSED al_types_timeout timeout)
{
    return BP_ENOTIMPL;
}

void bp_ud3tn_copy_eid(UNUSED al_types_endpoint_id *dst, UNUSED al_types_endpoint_id *const src)
{
}

al_bp_error_t
bp_ud3tn_parse_eid_string(UNUSED al_types_endpoint_id *out_eid, UNUSED const char *const str)
{
    return BP_ENOTIMPL;
}

al_bp_error_t
bp_ud3tn_set_payload(UNUSED al_types_bundle_payload *const payload, UNUSED al_types_bundle_payload_location location,
    UNUSED const char *const buf, UNUSED uint32_t buf_length)
{
    return BP_ENOTIMPL;
}

void bp_ud3tn_free_payload(UNUSED al_types_bundle_payload *payload)
{
}

void bp_ud3tn_free_extensions(UNUSED al_types_bundle_spec *spec)
{
}

al_bp_error_t
bp_ud3tn_error(UNUSED int code)
{
    return BP_ENOTIMPL;
}

al_bp_error_t
bp_ud3tn_find_registration(UNUSED al_types_handle handle,
    UNUSED al_types_endpoint_id *eid, al_types_reg_id *regid)
{
    return BP_ENOTIMPL;
}

al_bp_error_t
bp_ud3tn_unregister(UNUSED al_types_handle handle, UNUSED al_types_reg_id regid)
{
    return BP_ENOTIMPL;
}

al_bp_error_t
bp_ud3tn_errno(UNUSED al_types_handle handle)
{
    return BP_ENOTIMPL;
}

#endif /* UD3TN_IMPLEMENTATION */