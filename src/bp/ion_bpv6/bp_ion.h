/** \file bp_ion.h
 *
 * \brief  This file contains the prototypes of the functions implemented in the homonymous .c file.
 *
 *  \par Copyright
 *  	Copyright (c) 2013, Alma Mater Studiorum, University of Bologna. All rights reserved.
 *
 *  \par License
 *     This file is part of Unified-API.                                            	<br>
 *                                                                                	<br>
 *     Unified-API is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.                                        	<br>
 *     Unified-API is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.                               	<br>
 *                                                                                	<br>
 *     You should have received a copy of the GNU General Public License
 *     along with Unified-API.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  \authors Michele Rodolfi, michele.rodolfi@studio.unibo.it
 *  \authors Anna d'Amico, anna.damico2@studio.unibo.it
 *
 *  \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 *	\par Revision History
 *
 *  DD/MM/YY | AUTHOR          |  DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  01/01/13 | M. Rodolfi	   |  Initial Implementation.
 *  01/01/13 | A. D'Amico	   |  Initial Implementation.
 *
 */


#ifndef BP_ION_H_
#define BP_ION_H_

#include "al_types.h"
#include "al_bp.h"


#define IPNSCHEMENAME "ipn"
#define DTNSCHEMENAME "dtn"

boolean_t bp_ion_is_running();

al_bp_error_t bp_ion_attach();

al_bp_error_t bp_ion_open_with_IP(char * daemon_api_IP,
								int daemon_api_port,
								al_types_handle * handle);

al_bp_error_t bp_ion_errno(al_types_handle handle);

/* The local eid is built with different rule according to the type:
 * Client :
 * 		if the eid_destination is ipn:nn.ns then the local eid is:
 * 			ipn:ownNodeNbr.ownPid
 * 		if the eid_destination is dtn://name.dtn then the local eid is:
 * 			dtn://ownNodeNbr.dtn/service_tag
 * 	Server-IPN or Monitor-IPN :
 * 		the service tag is converted to long unsigned integer and the local eid is:
 * 			ipn:ownNodeNbr.service_tag
 * 	Server-DTN or Monitor-DTN:
 * 		the local eid is :
 * 			dtn://ownNodeNbr.dtn/service_tag
 * */
al_bp_error_t bp_ion_build_local_eid(al_types_endpoint_id* local_eid,
								const char* service_tag,
								al_types_scheme type);

/* This API register the eid and open the connection initializing the handle*/
al_bp_error_t bp_ion_register(al_types_handle * handle,
                        al_types_reg_info* reginfo,
                        al_types_reg_id* newregid);

al_bp_error_t bp_ion_find_registration(al_types_handle handle,
						al_types_endpoint_id * eid,
						al_types_reg_id * newregid);

al_bp_error_t bp_ion_unregister(al_types_endpoint_id eid);

al_bp_error_t bp_ion_send(al_types_handle handle,
                    al_types_reg_id regid,
                    al_types_bundle_spec* spec,
                    al_types_bundle_payload* payload);

al_bp_error_t bp_ion_recv(al_types_handle handle,
                    al_types_bundle_spec* spec,
                    al_types_bundle_payload_location location,
                    al_types_bundle_payload* payload,
                    al_types_timeout timeout);

al_bp_error_t bp_ion_close(al_types_handle handle);

void bp_ion_copy_eid(al_types_endpoint_id* dst, al_types_endpoint_id* src);

al_bp_error_t bp_ion_parse_eid_string(al_types_endpoint_id* eid, const char* str);

al_bp_error_t bp_ion_set_payload(al_types_bundle_payload* payload,
                           al_types_bundle_payload_location location,
                           char* val, int len);

void bp_ion_free_payload(al_types_bundle_payload* payload);

void bp_ion_free_extension_blocks(al_types_bundle_spec* spec);

void bp_ion_free_metadata_blocks(al_types_bundle_spec* spec);

void bp_ion_free_extensions(al_types_bundle_spec* spec);

/**
 * converts DTN errors in the corresponding al_bp_error_t values
 */
al_bp_error_t bp_ion_error(int err);

#endif /* BP_ION_H_ */
