/** \file al_utilities_debug_print.c
 *
 *  \brief  This file contains all the debug related functions.
 *
 *  \details These functions have to be called directly by the DTN applications.
 *
 *  \par Copyright
 *  	Copyright (c) 2021, Alma Mater Studiorum, University of Bologna. All rights reserved.
 *
 *  \par License
 *     This file is part of Unified-API.                                            	<br>
 *                                                                                	<br>
 *     Unified-API is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.                                        	<br>
 *     Unified-API is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.                               	<br>
 *                                                                                	<br>
 *     You should have received a copy of the GNU General Public License
 *     along with Unified-API.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * \authors Marco Bertolazzi, marco.bertolazzi3@studio.unibo.it
 * \authors Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 * \authors Silvia Lanzoni, silvia.lanzoni5@studio.unibo.it
 *
 *
 *  \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 *	\par Revision History
 *
 *  DD/MM/YY | AUTHOR          |  DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  01/01/20 | M. Bertolazzi   |  Initial Implementation.
 *  01/01/20 | A. Bisacchi	   |  Implementation refactoring.
 *  15/01/22 | S. Lanzoni      |  Add documentation.
 *
 */


#include "al_utilities_debug_print.h"

#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void al_utilities_debug_print_logger_va_list(FILE* fp, const char *string, va_list argList);

typedef struct {
	int debug;	//debug level
	boolean_t create_log;	//request log_file
	FILE* log_fp;	//file pointer
	sem_t lock;	//semaphore
} debugger_t;

static debugger_t debug_struct;
static boolean_t is_initialized = FALSE;
static boolean_t is_sem_initialized = FALSE;

/******************************************************************************
  *
  * \par Function Name:
  *      al_utilities_debug_print_debugger_init
  *
  * \brief  It starts the debugger print object.
  *
  * \return int
  *
  * \param[in] 	debug				debug level
  * \param[in]  create_log			request log_file
  * \param[in]  log_filename		log file name
  *
  *****************************************************************************/

int al_utilities_debug_print_debugger_init(int debug, boolean_t create_log, char *log_filename) {
	//check input parameters
	if (is_initialized || debug < 0) return EXIT_FAILURE;
	is_initialized = TRUE;

	//initialize the structure debugger_t
	memset(&debug_struct, 0, sizeof(debug_struct));
	debug_struct.debug = debug;
	debug_struct.create_log = create_log;

	//Initialize semaphore object
	if (!is_sem_initialized) {
		sem_init(&(debug_struct.lock), 0, 1);
		is_sem_initialized = TRUE;
	}

	//if create_log is TRUE, open log file
	if (debug_struct.create_log) {
		debug_struct.log_fp = fopen(log_filename, "w");
		if (debug_struct.log_fp == NULL) {
			return EXIT_FAILURE;
		}
	}
	return EXIT_SUCCESS;
}

/******************************************************************************
  *
  * \par Function Name:
  *      al_utilities_debug_print_debugger_destroy
  *
  * \brief  It destroys the debugger print object.
  *
  * \return int
  *
  *****************************************************************************/

int al_utilities_debug_print_debugger_destroy() {
	////check parameters
	if (!is_initialized) return EXIT_FAILURE;
	is_initialized = FALSE;

	//Free resources associated with semaphore object
	if (is_sem_initialized) {
		sem_destroy(&(debug_struct.lock));
		is_sem_initialized = FALSE;
	}

	//Close log file
	if (debug_struct.create_log) {
		if (fclose(debug_struct.log_fp) == EOF) {
			return EXIT_FAILURE;
		}
	}
	return EXIT_SUCCESS;
}

/******************************************************************************
  *
  * \par Function Name:
  *      al_utilities_debug_print_logger_va_list
  *
  * \brief  log file print function
  *
  * \return void
  *
  * \param[in] 	fp			file pointer
  * \param[in] string		string to write
  *
  ** \par Notes: This is a private function.
  *****************************************************************************/

void al_utilities_debug_print_logger_va_list(FILE* fp, const char *string, va_list argList) {
	if (fp == NULL) {
		printf("Failed to print debug\n");
		return;
	}

	if (!is_sem_initialized) {
		sem_init(&(debug_struct.lock), 0, 1);
		is_sem_initialized = TRUE;
	}
	sem_wait(&(debug_struct.lock));
//All these functions invoke va_arg at least once, the value of arg is
//indeterminate after the return.
//These functions do not invoke va_end, and it must be done by the caller.
//	https://en.cppreference.com/w/c/io/vfprintf
	vfprintf (fp, string, argList);
	fflush(fp);
	sem_post(&(debug_struct.lock));
	return;
}

/******************************************************************************
  *
  * \par Function Name:
  *      al_utilities_debug_print
  *
  * \brief  It print the message in input corresponding to the debug level chosen
  *
  * \return void
  *
  * \param[in] 	level				threshold_level
  * \param[in]  string				message to print
  *
  *****************************************************************************/

void al_utilities_debug_print(threshold_level level, const char *string, ...) {
	if (is_initialized && debug_struct.debug >= level) {
		va_list lp;
		va_start(lp, string);
		al_utilities_debug_print_logger_va_list(stdout, string, lp);
		// print log
		if (debug_struct.create_log)
			al_utilities_debug_print_logger_va_list(debug_struct.log_fp, string, lp);
		va_end(lp);
	}
}

/******************************************************************************
  *
  * \par Function Name:
  *      al_bundle_get_source
  *
  * \brief  al_utilities_debug_print_error
  *
  * \return void

  *****************************************************************************/

void al_utilities_debug_print_error(const char* string, ...) {
	va_list lp;
	va_start(lp, string);
	al_utilities_debug_print_logger_va_list(stderr, string, lp);
	va_end(lp);
	return;
}

/******************************************************************************
  *
  * \par Function Name:
  *      al_utilities_debug_print_get_log_fp
  *
  * \brief  It returns the log_fp
  *
  * \return FILE*
  *
  *****************************************************************************/

FILE* al_utilities_debug_print_get_log_fp() {
	if (debug_struct.create_log && is_initialized)
		return debug_struct.log_fp;
	else
		return NULL;
}

/******************************************************************************
  *
  * \par Function Name:
  *      al_utilities_debug_print_check_level
  *
  * \brief  It checks the debug level
  *
  * \return boolean_t
  *
  * \param[in] 	required_level		The required level
  *
  *****************************************************************************/

boolean_t al_utilities_debug_print_check_level(threshold_level required_level) {
	return (debug_struct.debug >= required_level);
}


