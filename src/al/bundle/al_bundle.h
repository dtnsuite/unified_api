/** \file al_bundle.h
 *
 *  \brief  This file contains the prototypes of the functions implemented in the homonymous .c file.
 *
 *  \par Copyright
 *  	Copyright (c) 2021, Alma Mater Studiorum, University of Bologna. All rights reserved.
 *
 *  \par License
 *     This file is part of Unified-API.                                            	<br>
 *                                                                                	<br>
 *     Unified-API is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.                                        	<br>
 *     Unified-API is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.                               	<br>
 *                                                                                	<br>
 *     You should have received a copy of the GNU General Public License
 *     along with Unified-API.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  \authors Michele Rodolfi, michele.rodolfi@studio.unibo.it
 *  \authors Anna d'Amico, anna.damico2@studio.unibo.it
 *  \authors Davide Pallotti, davide.pallotti@studio.unibo.it
 *  \authors Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 *  \authors Silvia Lanzoni, silvia.lanzoni5@studio.unibo.it
 *
 *  \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 *  \par Revision History
 *
 *  DD/MM/YY | AUTHOR          |  DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  01/01/13 | M. Rodolfi	   |  Initial Implementation.
 *  01/01/13 | A. D'Amico	   |  Initial Implementation.
 *  01/01/16 | D. Pallotti	   |  Implementation contribution.
 *  01/01/20 | A. Bisacchi	   |  Implementation contribution.
 *  01/10/21 | S. Lanzoni      |  Implementation refactoring and reorganization.
 */


#include "../types/al_types.h"

#ifndef AL_BUNDLE_H_
#define AL_BUNDLE_H_

//Create a bundle

al_error al_bundle_create(al_types_bundle_object * bundle_object);

//Free a bundle

al_error al_bundle_free(al_types_bundle_object * bundle_object);

//Get bundle id

al_error al_bundle_get_id(al_types_bundle_object bundle_object, al_types_bundle_id ** bundle_id);

// Unset Bundle payload

al_error al_bundle_unset_payload(al_types_bundle_object * bundle_object);

// Set Bundle payload location

al_error al_bundle_set_payload_location(al_types_bundle_object * bundle_object,
											al_types_bundle_payload_location location);
// Get Bundle payload location

al_error al_bundle_get_payload_location(al_types_bundle_object bundle_object,
										al_types_bundle_payload_location * location);


 // Get payload size if payload location is BP_PAYLOAD_FILE gets the size of the file

al_error al_bundle_get_payload_size(al_types_bundle_object bundle_object, uint32_t * size);


// Get filename of payload

al_error al_bundle_get_payload_file(al_types_bundle_object bundle_object,
										char ** filename, uint32_t * filename_len);

// Get pointer of payload buffer

al_error al_bundle_get_payload_mem(al_types_bundle_object bundle_object,
		char ** buf, uint32_t * buf_len);

//CCaini experimental
al_error al_bundle_set_payload(al_types_bundle_object *bundle_object,
		char *buffer_or_filename, uint32_t length, al_types_bundle_payload_location location);

// Set payload from a file

al_error al_bundle_set_payload_file(al_types_bundle_object * bundle_object,
										char * filename, uint32_t filename_len);

// Set payload from memory

al_error al_bundle_set_payload_mem(al_types_bundle_object * bundle_object,
										char * buf, uint32_t buf_len);

// Get bundle source EID

al_error al_bundle_get_source(al_types_bundle_object bundle_object, al_types_endpoint_id * source);

// Set Bundle Source EID

al_error al_bundle_set_source(al_types_bundle_object * bundle_object, al_types_endpoint_id source);

// Get bundle destination EID

al_error al_bundle_get_dest(al_types_bundle_object bundle_object, al_types_endpoint_id * dest);

// Set bundle destination EID

al_error al_bundle_set_dest(al_types_bundle_object * bundle_object, al_types_endpoint_id dest);

// Get bundle report-to EID

al_error al_bundle_get_report_to(al_types_bundle_object bundle_object, al_types_endpoint_id * report_to);

// Set bundle report-to EID

al_error al_bundle_set_report_to(al_types_bundle_object * bundle_object, al_types_endpoint_id report_to);

// Set bundle unreliable

al_error al_bundle_set_unreliable(al_types_bundle_object * bundle_object, boolean_t unreliable);

// Get bundle unreliable

al_error al_bundle_get_unreliable(al_types_bundle_object bundle_object, boolean_t * unreliable);

// Set bundle reliable

al_error al_bundle_set_reliable(al_types_bundle_object * bundle_object, boolean_t reliable);

// Get bundle reliable

al_error al_bundle_get_reliable(al_types_bundle_object bundle_object, boolean_t * reliable);

// Set bundle critical

al_error al_bundle_set_critical(al_types_bundle_object * bundle_object, boolean_t critical);

// Get bundle critical

al_error al_bundle_get_critical(al_types_bundle_object bundle_object, boolean_t * critical);

// Set bundle flow label

al_error al_bundle_set_flow_label(al_types_bundle_object * bundle_object, uint32_t flow_label);

// Get bundle flow label

al_error al_bundle_get_flow_label(al_types_bundle_object bundle_object, uint32_t * flow_label);

// Get bundle expiration time

al_error al_bundle_get_lifetime(al_types_bundle_object bundle_object, al_types_timeval * exp);

// Set bundle expiration time

al_error al_bundle_set_lifetime(al_types_bundle_object * bundle_object, al_types_timeval exp);

// Get bundle creation timestamp

al_error al_bundle_get_creation_timestamp(al_types_bundle_object bundle_object, al_types_creation_timestamp * ts);

// Set bundle creation timestamp

al_error al_bundle_set_creation_timestamp(al_types_bundle_object * bundle_object, al_types_creation_timestamp ts);

// Get bundle delivery options

al_error al_bundle_get_control_flags(al_types_bundle_object bundle_object,
											al_types_bundle_processing_control_flags * bundle_proc_ctrl_flags);
// Set bundle delivery options

al_error al_bundle_set_control_flags(al_types_bundle_object * bundle_object,
											al_types_bundle_processing_control_flags bundle_proc_ctrl_flags);

//get bundle status report
al_error al_bundle_get_status_report(al_types_bundle_object bundle_object,
											al_types_bundle_status_report ** status_report);

// Get cardinal bundle priority

al_error al_bundle_get_cardinal_priority(al_types_bundle_object bundle_object, al_types_bundle_priority_enum * priority);

// Set cardinal bundle priority

al_error al_bundle_set_cardinal_priority(al_types_bundle_object * bundle_object, al_types_bundle_priority_enum priority);

// Get ordinal bundle priority

al_error al_bundle_get_ordinal_priority(al_types_bundle_object bundle_object, uint32_t priority);

// Set ordinal bundle priority

al_error al_bundle_set_ordinal_priority(al_types_bundle_object * bundle_object, uint32_t priority);

// Set the inter-regional trace value

al_error al_bundle_set_irf_trace(al_types_bundle_object *bundle_object,unsigned char irf_trace);

//Get the inter-regional trace value

al_error al_bundle_get_irf_trace(al_types_bundle_object bundle_object, unsigned char irf_trace);

//Set bp version

al_error al_bundle_set_bp_version(al_types_bundle_object *bundle_object, uint8_t bp_version);

//Set extension block

al_error al_bundle_set_extension_block(al_types_bundle_object *bundle_object , char * metadata_type, char * metadata_string);

#endif /* AL_BUNDLE_H_*/
