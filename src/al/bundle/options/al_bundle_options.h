/** \file al_bundle_options.h
 *
 *  \brief  This file contains the definition of the parsing function implemented in the al_bundle_options.c file
 *
 *  \par Copyright
 *  	Copyright (c) 2021, Alma Mater Studiorum, University of Bologna. All rights reserved.
 *
 *  \par License
 *     This file is part of Unified-API.                                            	<br>
 *                                                                                	<br>
 *     Unified-API is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.                                        	<br>
 *     Unified-API is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.                               	<br>
 *                                                                                	<br>
 *     You should have received a copy of the GNU General Public License
 *     along with Unified-API.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *  \authors Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 *  \authors Federico Domenicali, federico.domenicali@studio.unibo.it
 *
 *  \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 *	\par Revision History
 *
 *  DD/MM/YY | AUTHOR          |  DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  01/01/13 | F. Domenicali   |  Initial Implementation.
 *  01/01/20 | A. Bisacchi	   |  Implementation contribution.
 *  01/01/22 | S. Lanzoni	   |  Implementation contribution.
 *
 */

#ifndef AL_BUNDLE_OPTIONS_H_
#define AL_BUNDLE_OPTIONS_H_

//#include <stdbool.h>

#include "unified_api_boolean_type.h"
//#include "unified_api_system_libraries.h"
//#include "al_bundle.h"
#include "../../types/al_types.h"

/**
 * \brief This is a structure used to group the bundle options.
 * The bp options are a subset of the bundle spec, the field order is the same.
 *
 */
typedef struct {
	uint8_t bp_version;
	al_types_timeval lifetime;
	al_types_bundle_priority_enum cardinal;
	boolean_t disable_fragmentation;
	boolean_t custody_transfer;
	boolean_t reception_reports;
	boolean_t custody_reports;
	boolean_t forwarding_reports;
	boolean_t delivery_reports;
	boolean_t deletion_reports;
	boolean_t ack_requested_by_application;
	al_types_bundle_ecos_t ecos;
	char * metadata_type; //uint8_t RFC 6258 seems to specify 255 as a max
	//not changed because a long chain of modifications would be required
	char * metadata_string;
	unsigned char irf_trace;
} bundle_options_t;

/**
 * \brief This is a structure used to group the fields which are not directly related to the bundle
 */

typedef struct {
	uint32_t ipn_local;
	char eid_format_forced;
	int debug_level;
	char ip [100];
	int port;
} dtn_suite_options_t;

/**
 * \brief This is a structure used to save all the options not recognized by the al_bp parser and which are sent to the application parser
 */

typedef struct application_options_t{
	int app_opts_size;
	char** app_opts;
} application_options_t;

//functions:

al_error 			al_bundle_options_parse(int argc, char **argv, bundle_options_t* bundle_options, dtn_suite_options_t* dtn_suite_options, application_options_t* result_options);
void 				al_bundle_options_free(application_options_t result_options);
char* 				al_bundle_options_get_bundle_help(void);
char* 				al_bundle_options_get_dtnsuite_help(void);
al_error			al_bundle_options_set(al_types_bundle_object *bundle, bundle_options_t bundle_options);

#endif /*AL_BUNDLE_OPTIONS_H_*/
