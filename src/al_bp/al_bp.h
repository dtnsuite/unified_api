/** \file al_bp.h
 *
 *  \brief  This file contains the prototypes of the functions implemented in the homonymous .c file.
 *
 *  \par Copyright
 *  	Copyright (c) 2013, Alma Mater Studiorum, University of Bologna. All rights reserved.
 *
 *  \par License
 *     This file is part of Unified-API.                                            	<br>
 *                                                                                	<br>
 *     Unified-API is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.                                        	<br>
 *     Unified-API is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.                               	<br>
 *                                                                                	<br>
 *     You should have received a copy of the GNU General Public License
 *     along with Unified-API.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  \authors Michele Rodolfi, michele.rodolfi@studio.unibo.it
 *  \authors Anna d'Amico, anna.damico2@studio.unibo.it
 *  \authors Silvia Lanzoni, silvia.lanzoni5@studio.unibo.it
 *
 *  \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 *	\par Revision History
 *
 *  DD/MM/YY | AUTHOR          |  DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  01/01/13 | M. Rodolfi	   |  Initial Implementation.
 *  01/01/13 | A. D'Amico	   |  Initial Implementation.
 *  01/10/21 | S. Lanzoni      |  Implementation refactoring and documentation.
 *
 */

#ifndef AL_BP_H_
#define AL_BP_H_

#include "../al/types/al_types.h"
#include <pthread.h>
#include <stdlib.h> //CCaini

/**
 * \brief al_bp layer error codes
 */
typedef enum al_bp_error_t
{
	/**
	 * \brief Successful ending
	 */
	BP_SUCCESS = 0,
	/**
	 * \brief General error code
	 */
	BP_ERRBASE,
	/**
	 * \brief Error, NO Bundle Protocol Implementation found
	 */
	BP_ENOBPI,
	/**
	 * \brief Error, multiple running Bundle Protocol Implementation found
	 */
	BP_EMULTIBPI,
	/**
	 * \brief  Error, Unified API was not compiled for the running BP implementation.
	 */
	BP_EBADBPI,
	/**
	 * \brief Error, invalid argument
	 */
	BP_EINVAL,
	/**
	 * \brief Error, operation on a null pointer
	 */
	BP_ENULLPNTR,
	/**
	 * \brief  Error on unregistration
	 */
	BP_EUNREG,
	/**
	 * \brief  Error in connecting to server
	 */
	BP_ECONNECT,
	/**
	 * \brief Error, timed out
	 */
	BP_ETIMEOUT,
	/**
	 * \brief  Error, payload / eid too large
	 */
	BP_ESIZE,
	/**
	 * \brief Error, element not found
	 */
	BP_ENOTFOUND,
	/**
	 * \brief Error, internal error
	 */
	BP_EINTERNAL,
	/**
	 * \brief Error, registration already in use
	 */
	BP_EBUSY,
	/**
	 * \brief Error, no storage space
	 */
	BP_ENOSPACE,
	/**
	 * \brief  Error, function not yet implemented
	 */
	BP_ENOTIMPL,
	/**
	 * \brief  Error to attach bp protocol
	 */
	BP_EATTACH,
	/**
	 * \brief  Error on building a local EID
	 */
	BP_EBUILDEID,
	/**
	 * \brief  Error on open
	 */
	BP_EOPEN,
	/**
	 * \brief  Error on register a EID
	 */
	BP_EREG,
	/**
	 * \brief  Error to parse a endpoint id string
	 */
	BP_EPARSEEID,
	/**
	 * \brief  Error to send a bundle
	 */
	BP_ESEND,
	/**
	 * \brief  Error to receive a bundle
	 */
	BP_ERECV,
	/**
	 * \brief  Error on reception
	 */
	BP_ERECVINT
} al_bp_error_t;

/**
 * \brief Get Unified API library version.
 */
const char * get_unified_api_version();

al_bp_error_t al_bp_get_implementation();
char const *al_bp_get_implementation_name(void);

al_bp_error_t al_bp_get_default_scheme_running_bpi(al_types_scheme *scheme);

al_bp_error_t al_bp_lookup_option_compatibility(int option_index, boolean_t *result);
al_bp_error_t al_bp_lookup_implementation_supporting_option(int option_index, char *buf, int buf_size);

al_bp_error_t al_bp_open(al_types_handle* handle);

al_bp_error_t al_bp_open_with_ip(char *daemon_api_IP,int daemon_api_port,al_types_handle* handle);

al_bp_error_t al_bp_errno(al_types_handle handle);

al_bp_error_t al_bp_build_local_eid(al_types_handle handle,
									al_types_endpoint_id* local_eid,
									int ipn_node_number, int ipn_service_number,
									char *dtn_demux_string, 
									al_types_scheme type);


al_bp_error_t al_bp_register(al_types_handle * handle,
						al_types_reg_info* reginfo,
						al_types_reg_id* newregid, 
						pthread_mutex_t *mutex_half_duplex);


al_bp_error_t al_bp_find_registration(al_types_handle handle,
							al_types_endpoint_id * eid,
							al_types_reg_id * newregid);

//  al_bp_error_t al_bp_unregister(al_types_handle handle,
// 							al_types_reg_id regid,
// 							al_types_endpoint_id eid,
// 							pthread_mutex_t *mutex_half_duplex);


al_bp_error_t al_bp_send(al_types_handle handle,
					al_types_reg_id regid,
					al_types_bundle_spec* spec,
					al_types_bundle_payload* payload,
					pthread_mutex_t *mutex_half_duplex);


al_bp_error_t al_bp_recv(al_types_handle handle,
					al_types_bundle_spec* spec,
					al_types_bundle_payload_location location,
					al_types_bundle_payload* payload,
					al_types_timeout timeout,
					pthread_mutex_t *mutex_half_duplex);


// al_bp_error_t al_bp_close(al_types_handle handle);

al_bp_error_t al_bp_close_and_unregister(al_types_handle handle,
						al_types_reg_id regid,
						al_types_endpoint_id eid,
						pthread_mutex_t *mutex_half_duplex);

/*************************************************************
 *
 *                     Utility Functions
 *
 *************************************************************/

void al_bp_copy_eid(al_types_endpoint_id* dst, al_types_endpoint_id* src);

al_bp_error_t al_bp_get_none_endpoint(al_types_endpoint_id * eid_none);

al_bp_error_t al_bp_set_payload(al_types_bundle_payload* payload,
							al_types_bundle_payload_location location,
                           char* val, int len);

void al_bp_free_extensions(al_types_bundle_spec* spec);

void al_bp_free_payload(al_types_bundle_payload* payload);

char * al_bp_strerror(int err);

#endif /* AL_BP_H_ */
