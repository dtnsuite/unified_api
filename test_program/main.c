#include <unified_api.h>

void print_ext_or_metadata_blocks(uint32_t blocks_len, al_types_extension_block *blocks_val);


// server endpoint demux string //CCaini demux without delimiter "/" as in RFC9171
#define APP_DEMUX          "test"
#define APP_SERVICE_NUMBER 3
#define TIMEOUT_MS         5 * 1000
#define MAX_STRING_LENGTH  256

int main(int argc, char **argv)
{

    bundle_options_t bundle_options;        // options of the BP
    dtn_suite_options_t dtn_suite_options;  // parsed options common to all DTNsuite applications
    application_options_t residual_options; // options not parsed (i.e. those specific to the present application)
    al_socket_registration_descriptor rd;   // logically equivalent to socket/file descriptors
    al_error result;
    al_types_bundle_object bundle_out, bundle_in;
    al_types_endpoint_id source, dest, reportTo;
    int numberOfBundles = 4;
    uint32_t buf_len;
    char *buffer;
//    char filePath[100]; //alternative for payload in a file

    int module;

    result = al_socket_init(); // It initializes the al_socket
    if (result) {              // example of error management
        exit(1);
    }
    // Parsing of BP options and DTNsuite options; it also sets defaults
    al_bundle_options_parse(argc, argv, &bundle_options, &dtn_suite_options, &residual_options);
    // Initialize the debug print module
    al_utilities_debug_print_debugger_init(dtn_suite_options.debug_level, false, "test_log");
    // Register the application (either "dtn" or "ipn" EID; automatically selected unless forced)
    result = al_socket_register(&rd, APP_DEMUX, APP_SERVICE_NUMBER, dtn_suite_options.eid_format_forced, dtn_suite_options.ipn_local, dtn_suite_options.ip, dtn_suite_options.port);
    if (result) { // example of error management
        al_utilities_debug_print_debugger_destroy();
        al_socket_destroy();
        exit(1);
    }

    // Create the bundle structures
    al_bundle_create(&bundle_out);
    al_bundle_create(&bundle_in);
    // Set BP options in bundle
    al_bundle_options_set(&bundle_out, bundle_options);

    // This function returns the local EID structure associated to the registration descriptor given in input.
    source = al_socket_get_local_eid(rd);
    dest = source;                    // send the bundle to itself
//strcpy(dest.uri,"ipn:2.2"); //send the bundle to another destination
    strcpy(reportTo.uri, "dtn:none"); // initialize reportTo
    /*
                            bundle_out.payload->location = BP_PAYLOAD_MEM;
                            char* string_payload = "hello";
                            bundle_out.payload->buf.buf_len = strlen(string_payload);
                            bundle_out.payload->buf.buf_val = malloc(sizeof(char) * (bundle_out.payload->buf.buf_len + 1));
                            strcpy(bundle_out.payload->buf.buf_val, string_payload);
    */



    buffer = malloc(MAX_STRING_LENGTH + 1);
    char *string_payload[] = { "hello", "ciao", "byebye" };
    // char *filenames[] = {"payloadA.txt", "payloadB.txt", "payloadC.txt"};
    module = sizeof(string_payload) / sizeof(string_payload[0]); // module is the length of the array
    for (int i = 0; i < numberOfBundles; i++) {
        strcpy(buffer, string_payload[i % module]);
        buf_len = strlen(buffer) + 1;
        al_bundle_set_payload (&bundle_out, buffer, buf_len, BP_PAYLOAD_MEM);
//deprecated        al_bundle_set_payload_mem(&bundle_out, buffer, buf_len);

/* alternative if payload is in a file
//        strcpy(filePath,"/home/carlo/ion/prova1");
//       printf ("buf_len is %d but is not used\n", buf_len);
//        al_bundle_set_payload(&bundle_out, filePath, strlen(filePath), BP_PAYLOAD_FILE);
// deprecated       al_bundle_set_payload_file(&bundle_out, filePath, strlen(filePath));
*/
        al_socket_send(rd, bundle_out, dest, reportTo);
        al_utilities_debug_print(DEBUG_L1, "[DEBUG_L1] Sent bundle with message: %s  to EID %s\n", bundle_out.payload->buf.buf_val, dest.uri);

        while (true) { // to manage warnings that require the al_socket_received to be called again
            // timeout = BP_INFINITE_WAIT or BP_NO_WAIT or value
            result = al_socket_receive(rd, &bundle_in, BP_PAYLOAD_MEM, TIMEOUT_MS);
            if (result == AL_SUCCESS) { // a bundle has arrived before al_socket_receive timeout
                break;                  // exit the infinite loop
            } else if (result == AL_ERROR) {
                printf("[ERROR]: Error on al_socket_receive \n");
                exit(1);
            } else if (result == AL_WARNING_RECEPINTER) {
                al_utilities_debug_print(DEBUG_L2, "[DEBUG_L2] warning: reception interrupted \n");
            } else if (result == AL_WARNING_TIMEOUT) {
                al_utilities_debug_print(DEBUG_L2, "[DEBUG_L2] warning: al_bundle_receiver timed out \n");
            }
        } // end while: you must have received a bundle
        al_utilities_debug_print(DEBUG_L0,"Received bundle i %d, with message: %s from EID: %s\n", i, bundle_in.payload->buf.buf_val, bundle_in.spec->source.uri);
 //       printf("Received bundle i %d, with message: %s from EID: %s\n", i, bundle_in.payload->buf.buf_val, bundle_in.spec->source.uri);
        // al_utilities_bundle_print_bundle_object(bundle_in, "bundle", 1, stdout) ;
        // al_prog_print_ext_or_metadata_blocks(bundle_in.spec->extensions.extension_number, bundle_in.spec->extensions.extension_blocks);
    	if(bundle_in.spec->extensions.extension_number > 0)
    		print_ext_or_metadata_blocks(bundle_in.spec->extensions.extension_number, bundle_in.spec->extensions.extension_blocks);

    } // end for

    al_bundle_unset_payload(&bundle_out); // detach payload

    free(buffer);
    buffer = NULL;

    // Free memory allocated within al_bundle_create()
    al_bundle_free(&bundle_out);
    al_bundle_free(&bundle_in);

    al_bundle_options_free(residual_options) ;//Free memory allocated in al_bundle_options

    // Unregsiter the application
    al_socket_unregister(rd);
    // Destroy the debug print module
    al_utilities_debug_print_debugger_destroy();
    // Last destroy the registration list.
    al_socket_destroy();
    exit(0);
}

void print_ext_or_metadata_blocks(uint32_t blocks_len, al_types_extension_block *blocks_val)
{
	int i=0;
	al_utilities_debug_print(DEBUG_L1,"[DTNperf L1] number of blocks: %lu\n", blocks_len);
	for (i = 0; i < blocks_len; i++)
	{
		al_utilities_debug_print(DEBUG_L1,"[DEBUG_L1] Block[%d]\n", i);
		al_utilities_debug_print(DEBUG_L1,"[DEBUG_L1]---type: %lu\n", blocks_val[i].block_type_code);
		al_utilities_debug_print(DEBUG_L1,"[DEBUG_L1]---flags: %lu\n", blocks_val[i].block_processing_control_flags);
		al_utilities_debug_print(DEBUG_L1,"[DEBUG_L1]---data_len: %lu\n", blocks_val[i].block_data.block_type_specific_data_len);
		al_utilities_debug_print(DEBUG_L1,"[DEBUG_L1]---data_val: %s\n", blocks_val[i].block_data.block_type_specific_data);
	}
	return;
}
